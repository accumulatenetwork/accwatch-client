import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:grpc_web_ks/notification_data.dart';
import 'package:grpc_web_ks/provider/menu_provider.dart';
import 'package:grpc_web_ks/service.dart';
import 'package:provider/provider.dart';
import '../gRPC/AccWatch.pbgrpc.dart';
import 'package:url_launcher/url_launcher.dart';
import '../screens/AlertMessage.dart';

class TabSetting extends StatefulWidget {
  TabSetting({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _TabSettingState createState() => _TabSettingState();
}

class _TabSettingState extends State<TabSetting> {
  String _message = "nothing received yet";

  Settings nodeResult;
  MenuProvider _menuProvider;

  @override
  void initState() {
    super.initState();
    _menuProvider = Provider.of<MenuProvider>(context, listen: false);
    getSetting();
  }

  Future<Settings> getSetting() async {
    _menuProvider.isLoading = true;
    nodeResult = await DataService().getSetting(context);
    _menuProvider.isLoading = false;
    setState(() {});
  }

  settingSet(Settings value) async {
    MsgReply result = await DataService().settingSet(context,value);
    if (result.status == MsgReply_Status.ok) {
      getSetting();
    }
    else
        {
            showDialog(
                  context: context,
                  builder: (BuildContext dialogContext) {
                    return AlertMessage(title: "Settings failed", content: result.message);
                  },
                );
        }
  }

  void launchURL(String _url) async {
    if (!await launch(_url)) throw 'Could not launch $_url';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            height: MediaQuery.of(context).size.height - 100,
            child: Padding(
              padding: const EdgeInsets.all(10),
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Column(
                    children: [
                      Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Row(
                            children: [
                              ...settingTitle
                                  .asMap()
                                  .map((key, value) => MapEntry(
                                      key,
                                      Container(
                                        decoration: boxDecoration(0),
                                        height: 30,
                                        width: key == 0 ? 250 : 350,
                                        alignment: Alignment.center,
                                        child: Text(
                                          value,
                                          style: TextStyle(
                                              fontWeight: FontWeight.w800),
                                        ),
                                      )))
                                  .values
                                  .toList()
                            ],
                          ),
                          Column(
                            children: [
                              if (nodeResult != null)
                                Column(
                                  children: [
                                    Row(
                                      children: [
                                        wdKeyContent(settingKey[0]),
                                        wdSContent(nodeResult.botName,
                                            onChange: (value) {
                                          nodeResult.botName = value;
                                        }),
                                        wdKeyContent(SettingComment[0],
                                            width: 350)
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        wdKeyContent(settingKey[1]),
                                        wdSContent(nodeResult.discordClientID,
                                            onChange: (value) {
                                          nodeResult.discordClientID = value;
                                        }),
                                        wdKeyContent(
                                            "https://discordapp.com/oauth2/authorize?&client_id=" +
                                                nodeResult.discordClientID +
                                                "&scope=bot&permissions=1563712",
                                            width: 350, onChange: () {
                                          launchURL(
                                              "https://discordapp.com/oauth2/authorize?&client_id=" +
                                                  nodeResult.discordClientID +
                                                  "&scope=bot&permissions=1563712");
                                        })
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        wdKeyContent(settingKey[2]),
                                        wdSContent(nodeResult.discordToken,
                                            onChange: (value) {
                                          nodeResult.discordToken = value;
                                        }),
                                        wdKeyContent(SettingComment[2],
                                            width: 350)
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        wdKeyContent(settingKey[3]),
                                        wdSContent(
                                            nodeResult
                                                .accumulateOperatorAlertsCh
                                                .toString(), onChange: (value) {
                                          nodeResult
                                                  .accumulateOperatorAlertsCh =
                                              value;
                                        }),
                                        wdKeyContent(SettingComment[3],
                                            width: 350)
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        wdKeyContent(settingKey[4]),
                                        wdSContent(
                                            nodeResult.discordAlertsChannel,
                                            onChange: (value) {
                                          nodeResult.discordAlertsChannel =
                                              value;
                                        }),
                                        wdKeyContent(SettingComment[4],
                                            width: 350)
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        wdKeyContent(settingKey[5]),
                                        wdSContent(nodeResult.sIPUsername,
                                            onChange: (value) {
                                          nodeResult.sIPUsername = value;
                                        }),
                                        wdKeyContent(SettingComment[5],
                                            width: 350)
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        wdKeyContent(settingKey[6]),
                                        wdSContent(nodeResult.sIPPassword,
                                            onChange: (value) {
                                          nodeResult.sIPPassword = value;
                                        }),
                                        wdKeyContent(SettingComment[6],
                                            width: 350)
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        wdKeyContent(settingKey[7]),
                                        wdSContent(nodeResult.sIPHost,
                                            onChange: (value) {
                                          nodeResult.sIPHost = value;
                                        }),
                                        wdKeyContent(SettingComment[7],
                                            width: 350)
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        wdKeyContent(settingKey[8]),
                                        wdSContent(nodeResult.sIPCallingNumber,
                                            onChange: (value) {
                                          nodeResult.sIPCallingNumber = value;
                                        }),
                                        wdKeyContent(SettingComment[8],
                                            width: 350)
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        wdKeyContent(settingKey[9]),
                                        wdSContent(nodeResult.twimletURL,
                                            onChange: (value) {
                                          nodeResult.twimletURL = value;
                                        }),
                                        wdKeyContent(SettingComment[9],
                                            width: 350)
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        wdKeyContent(settingKey[10]),
                                        wdSContent(
                                            nodeResult.alarmOffWarningMinutes
                                                .toString(), onChange: (value) {
                                          if (value == "") value = "0";
                                          nodeResult.alarmOffWarningMinutes =
                                              int.parse(value);
                                        }),
                                        wdKeyContent(SettingComment[10],
                                            width: 350)
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        wdKeyContent(settingKey[11]),
                                        wdSContent(
                                            nodeResult.latencyTriggerMultiplier
                                                .toString(), onChange: (value) {
                                          if (value == "") value = "0";
                                          nodeResult.latencyTriggerMultiplier =
                                              int.parse(value);
                                        }),
                                        wdKeyContent(SettingComment[11],
                                            width: 350)
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        wdKeyContent(settingKey[12]),
                                        wdSContent(settingKey[12],
                                            onChange: (value) {
                                          nodeResult.twimletURL = value;
                                        }),
                                        wdKeyContent(SettingComment[12],
                                            width: 350)
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        wdKeyContent(settingKey[13]),
                                        wdSContent(settingKey[13],
                                            onChange: (value) {}),
                                        wdKeyContent(SettingComment[13],
                                            width: 350)
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        wdKeyContent(settingKey[14]),
                                        wdSContent(settingKey[14],
                                            onChange: (value) {}),
                                        wdKeyContent(SettingComment[14],
                                            width: 350)
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        wdKeyContent(settingKey[15]),
                                        wdSContent(nodeResult.botCommandPrefix,
                                            onChange: (value) {
                                          nodeResult.botCommandPrefix = value;
                                        }),
                                        wdKeyContent(SettingComment[15],
                                            width: 350)
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        wdKeyContent(settingKey[16]),
                                        wdSContent(nodeResult.emailSMTPHost,
                                            onChange: (value) {
                                          nodeResult.emailSMTPHost = value;
                                        }),
                                        wdKeyContent(SettingComment[16],
                                            width: 350)
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        wdKeyContent(settingKey[17]),
                                        wdSContent(
                                            nodeResult.emailSMTPPort.toString(),
                                            onChange: (value) {
                                          if (value == "") value = "0";
                                          nodeResult.emailSMTPPort =
                                              int.parse(value);
                                        }),
                                        wdKeyContent(SettingComment[17],
                                            width: 350)
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        wdKeyContent(settingKey[18]),
                                        wdSContent(nodeResult.emailUsername,
                                            onChange: (value) {
                                          nodeResult.emailUsername = value;
                                        }),
                                        wdKeyContent(SettingComment[18],
                                            width: 350)
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        wdKeyContent(settingKey[19]),
                                        wdSContent(nodeResult.emailPassword,
                                            onChange: (value) {
                                          nodeResult.emailPassword = value;
                                        }),
                                        wdKeyContent(SettingComment[19],
                                            width: 350)
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        wdKeyContent(settingKey[20]),
                                        wdSContent(nodeResult.emailFromAddress,
                                            onChange: (value) {
                                          nodeResult.emailFromAddress = value;
                                        }),
                                        wdKeyContent(SettingComment[20],
                                            width: 350)
                                      ],
                                    )
                                  ],
                                ),
                              if (nodeResult == null)
                                ...nodes
                                    .asMap()
                                    .map((key, value) {
                                      return MapEntry(
                                          key,
                                          Row(
                                            children: [
                                              wdContentEmpty(width: 250),
                                              wdContentEmpty(width: 350),
                                              wdContentEmpty(width: 350),
                                            ],
                                          ));
                                    })
                                    .values
                                    .toList()
                            ],
                          ),
                        ],
                      ),
                      Container(
                        padding: EdgeInsets.only(top: 30, left: 30, right: 20),
                        child: Row(
                          children: [
                            Container(
                              width: 350,
                              child: TextFormField(
                                  maxLines: 6,
                                  decoration: InputDecoration(
                                    border: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.greenAccent,
                                          width: 5.0),
                                    ),
                                    hintText: "please input note",
                                  )),
                            ),
                            SizedBox(width: 20),
                            Container(
                                child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                SizedBox(
                                  width: 160.0,
                                  child: RaisedButton(
                                    onPressed: () {
                                      settingSet(nodeResult);
                                    },
                                    child: Text(
                                      "Save",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                    color: const Color(0xFF1BC0C5),
                                  ),
                                ),
                                SizedBox(
                                  width: 10,
                                ),
                                SizedBox(
                                  width: 160.0,
                                  child: RaisedButton(
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                    child: Text(
                                      "Cancel",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                    color: const Color(0xFFFFC0C5),
                                  ),
                                )
                              ],
                            ))
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
          _menuProvider.isLoading?
          SpinKitSpinningLines(color: Colors.blue[400]):Container()

        ],
      ),
    );
  }

  BoxDecoration boxDecoration(int position) {
    return BoxDecoration(
        color: position == 0 ? Colors.blueGrey.shade100 : Colors.white,
        border: Border(
            top: position == 1
                ? BorderSide.none
                : BorderSide(color: Colors.grey),
            right: BorderSide(color: Colors.grey),
            bottom: BorderSide(color: Colors.grey),
            left: BorderSide(color: Colors.grey)));
  }

  Widget wdKeyContent(String value,
      {Function onChange, int type = 1, double width = 250}) {
    return GestureDetector(
      onTap: onChange,
      child: Container(
        decoration: boxDecoration(type),
        width: width,
        height: 30,
        alignment: Alignment.center,
        padding: const EdgeInsets.only(left: 5),
        child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Text(value),
        ),
      ),
    );
  }

  Widget wdSContent(String value,
      {Function onChange, int type = 1, bool isNumber = false}) {
    return Container(
      decoration: boxDecoration(type),
      width: 350,
      height: 30,
      alignment: Alignment.center,
      padding: const EdgeInsets.only(left: 5),
      child: TextFormField(
        initialValue: value.toString(),
        onChanged: onChange,
      ),
    );
  }

  Widget wdContent(DomainCertificate node, int type, String value) {
    return GestureDetector(
      child: Container(
        decoration: boxDecoration(type),
        width: 150,
        height: 25,
        alignment: Alignment.center,
        padding: const EdgeInsets.only(left: 5),
        child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Text(value),
        ),
      ),
    );
  }

  Widget wdContentEmpty({double width = 150, double height = 30}) {
    return GestureDetector(
      child: Container(
        decoration: boxDecoration(1),
        width: width,
        height: height,
        alignment: Alignment.center,
        padding: const EdgeInsets.only(left: 5),
      ),
      onTap: () async {},
    );
  }

  Widget wdCheckContent(int type, String value) {
    return GestureDetector(
      child: Container(
        decoration: boxDecoration(type),
        width: 150,
        height: 25,
        alignment: Alignment.center,
        child: Icon(value == "true"
            ? Icons.check_box_outlined
            : Icons.check_box_outline_blank),
      ),
    );
  }
}
