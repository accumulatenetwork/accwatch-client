import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import '../gRPC/AccWatch.pbgrpc.dart';
import '../screens/AlertMessage.dart';

class EditNetworkDialog extends StatefulWidget {
  const EditNetworkDialog({this.node, this.isNew, Key key }) : super(key: key);
  final Network node;
  final bool isNew;

  @override
  State<EditNetworkDialog> createState() => _EditNetworkDialogState();
}

class _EditNetworkDialogState extends State<EditNetworkDialog> {
      String tmpName ;
      int tmpBlockTime;
      int tmpStalledAfter;
      int tmpNotificationId;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    tmpName = widget.node.name;
    tmpBlockTime = widget.node.blockTime;
    tmpStalledAfter = widget.node.stalledAfter;
    tmpNotificationId = widget.node.notifictionID;
  }
  @override
  Widget build(BuildContext context) {
    return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius:
                    BorderRadius.circular(20.0)), //this right here
            child: Container(
              height: 400,
              width: 600,
              child: Padding(
                padding: const EdgeInsets.all(12.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                      Container(child: Text("Network Name"),),
                      Container(width: 350,child: TextFormField(
                        initialValue: tmpName.toString(),
                        keyboardType: TextInputType.text,
                        onChanged: (value){
                          widget.node.name=value;
                        },
                      ))
                    ],),
                    Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,children: [
                      Container(child: Text("Block Time(sec)"),),
                      Container(width: 350,child: TextFormField(
                        initialValue: tmpBlockTime.toString(),
                        keyboardType: TextInputType.number,
                        inputFormatters: [FilteringTextInputFormatter.allow(RegExp('[0-9.,-]')),],
                        onChanged: (value){
                          if(value!="")
                          widget.node.blockTime=int.parse(value);
                        },
                      ))
                    ],),
                    Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,children: [
                      Container(child: Text("Stalled After(Sec)"),),
                      Container(width: 350,child: TextFormField(
                        initialValue: tmpStalledAfter.toString(),
                        keyboardType: TextInputType.number,
                        inputFormatters: [FilteringTextInputFormatter.allow(RegExp('[0-9.,-]')),],
                        onChanged: (value){
                          if(value!="")
                          widget.node.stalledAfter = int.parse(value);
                        },
                      ))
                    ],),
                    SizedBox(height: 10),
                    Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,children: [
                      Container(child: Text("Stall Notification"),),
                      Container(width: 350,child: TextFormField(
                        initialValue: tmpNotificationId.toString(),
                        keyboardType: TextInputType.text,
                        inputFormatters: [FilteringTextInputFormatter.allow(RegExp('[0-9.,-]')),],
                        onChanged: (value){
                          if(value!="")
                          widget.node.notifictionID = int.parse(value);
                        },
                      ))
                    ],),
                    
                    SizedBox(height: 10),
                    Padding(padding: EdgeInsets.fromLTRB(30, 20, 30, 10),
                    child: Row(mainAxisAlignment: MainAxisAlignment.spaceAround,children: [
                      SizedBox(
                      width: 160.0,
                      child: RaisedButton(
                        onPressed: widget.isNew?null:() {
                          Navigator.of(context).pop({"node": widget.node,"remove":true});
                        },
                        child: Icon(Icons.delete_forever),
                        color: Colors.red,
                      ),
                    ),
                      SizedBox(
                      width: 160.0,
                      child: RaisedButton(
                        onPressed: () {
                          Navigator.of(context).pop({"node":widget.node});
                        },
                        child: Text(
                          "Save",
                          style: TextStyle(color: Colors.white),
                        ),
                        color: const Color(0xFF1BC0C5),
                      ),
                    ),
                    SizedBox(
                      width: 160.0,
                      child: RaisedButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          "Cancel",
                          style: TextStyle(color: Colors.white),
                        ),
                        color: const Color(0xFFFFC0C5),
                      ),
                    )
                    ],),)
                  ],
                ),
              ),
            ),
          );
  }
}