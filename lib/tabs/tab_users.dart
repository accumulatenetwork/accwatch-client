import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:grpc/grpc_web.dart';
import 'package:grpc_web_ks/provider/menu_provider.dart';
import 'package:grpc_web_ks/service.dart';
import 'package:grpc_web_ks/widget/wd_content.dart';
import 'package:provider/provider.dart';

import '../gRPC/AccWatch.pbgrpc.dart';
import 'package:grpc_web_ks/notification_data.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:grpc_web_ks/global.dart';
import '../screens/AlertMessage.dart';

class TabUsers extends StatefulWidget {
  TabUsers({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _TabUsersState createState() => _TabUsersState();
}

class _TabUsersState extends State<TabUsers> {
  String _message = "nothing received yet";

  UserList nodeResult;

  MenuProvider _menuProvider;

  @override
  void initState() {
    super.initState();
    _menuProvider = Provider.of<MenuProvider>(context, listen: false);
    getUserList();
  }

  getUserList({bool isRefresh = false}) async {
    _menuProvider.isLoading = true;
    UserList result = await DataService().userListGet(context);
    _menuProvider.isLoading = false;
    setState(() {
      nodeResult = result;
    });
  }

  Future<MsgReply> userSet(User value) async {
    final channel =
        GrpcWebClientChannel.xhr(Uri.parse(apiURL));
    final client = AccWatchAPIClient(channel);
    MsgReply result = await client.userSet(User(
        userID: value.userID ?? 0,
        tel: value.tel,
        name: value.name,
        discord: value.discord,
        email: value.email));
    if (result.status == MsgReply_Status.ok) {
      getUserList();
    }
    else
        {
            showDialog(
                  context: context,
                  builder: (BuildContext dialogContext) {
                    return AlertMessage(title: "User Set failed", content: result.message);
                  },
                );
        }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Padding(
            padding: const EdgeInsets.all(10),
            child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        children: [
                          ...userTitle
                              .asMap()
                              .map((key, value) => MapEntry(
                                  key,
                                  Container(
                                    decoration: boxDecoration(0),
                                    height: 30,
                                    width: 150,
                                    alignment: Alignment.center,
                                    child: Text(
                                      value,
                                      style: TextStyle(
                                          fontWeight: FontWeight.w800),
                                    ),
                                  )))
                              .values
                              .toList()
                        ],
                      ),
                      Column(
                        children: [
                          if (nodeResult != null)
                            ...nodeResult.users
                                .asMap()
                                .map((key, value) {
                                  return MapEntry(
                                      key,
                                      InkWell(
                                        onTap: () async {
                                          var result =
                                              await showEditBox(context, value);
                                          if (result != null) {
                                            userSet(result['node']);
                                          }
                                        },
                                        highlightColor: Colors.red[100],
                                        hoverColor: Colors.red[50],
                                        child: Row(
                                          children: [
                                            wdContent(
                                                0, value.discord.toString()),
                                            wdContent(1, value.name.toString()),
                                            wdContent(2, value.tel.toString()),
                                            wdContent(
                                                2, value.email.toString()),
                                          ],
                                        ),
                                      ));
                                })
                                .values
                                .toList(),
                          if (nodeResult == null)
                            ...nodes
                                .asMap()
                                .map((key, value) {
                                  return MapEntry(
                                      key,
                                      Row(
                                        children: [
                                          wdContentEmpty(),
                                          wdContentEmpty(),
                                          wdContentEmpty(),
                                          wdContentEmpty(),
                                        ],
                                      ));
                                })
                                .values
                                .toList()
                        ],
                      ),
                    ],
                  ),
                )),
          ),
           _menuProvider.isLoading?
          SpinKitSpinningLines(color: Colors.blue[400]):Container()
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          var result = await showEditBox(context, User());
          if (result != null) {
            userSet(result['node']);
          }
        },
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }



  BoxDecoration boxDecoration(int position) {
    return BoxDecoration(
        color: position == 0 ? Colors.blueGrey.shade100 : Colors.white,
        border: Border(
            top: position == 1
                ? BorderSide.none
                : BorderSide(color: Colors.grey),
            right: BorderSide(color: Colors.grey),
            bottom: BorderSide(color: Colors.grey),
            left: BorderSide(color: Colors.grey)));
  }

  showEditBox(BuildContext context, User node) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return EditDialog(node: node);
        });
  }
}

class EditDialog extends StatefulWidget {
  const EditDialog({this.node, Key key}) : super(key: key);
  final User node;

  @override
  State<EditDialog> createState() => _EditDialogState();
}

class _EditDialogState extends State<EditDialog> {
  String tmpDiscord;
  String tmpTel;
  String tmpName;
  String tmpEmail;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    tmpDiscord = widget.node.discord ?? false;
    tmpTel = widget.node.tel;
    tmpName = widget.node.name;
    tmpEmail = widget.node.email;
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20.0)), //this right here
      child: Container(
        height: 400,
        width: 600,
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    child: Text("Discord ID"),
                  ),
                  Container(
                      width: 350,
                      child: TextFormField(
                        initialValue: tmpDiscord.toString(),
                        keyboardType: TextInputType.text,
                        onChanged: (value) {
                          widget.node.discord = value;
                        },
                      ))
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    child: Text("Name"),
                  ),
                  Container(
                      width: 350,
                      child: TextFormField(
                        initialValue: tmpName.toString(),
                        keyboardType: TextInputType.text,
                        onChanged: (value) {
                          widget.node.name = value;
                        },
                      ))
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    child: Text("Tel"),
                  ),
                  Container(
                      width: 350,
                      child: TextFormField(
                        initialValue: tmpTel.toString(),
                        keyboardType: TextInputType.number,
                        inputFormatters: [
                          FilteringTextInputFormatter.allow(
                              RegExp('[0-9.,-/+]')),
                        ],
                        onChanged: (value) {
                          widget.node.tel = value;
                        },
                      ))
                ],
              ),
              SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    child: Text("e-mail"),
                  ),
                  Container(
                      width: 350,
                      child: TextFormField(
                        initialValue: tmpEmail.toString(),
                        keyboardType: TextInputType.text,
                        onChanged: (value) {
                          widget.node.email = value;
                        },
                      ))
                ],
              ),
              SizedBox(height: 10),
              Padding(
                padding: EdgeInsets.fromLTRB(30, 20, 30, 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    SizedBox(
                      width: 160.0,
                      child: RaisedButton(
                        onPressed: () {
                          Navigator.of(context).pop({"node": widget.node});
                        },
                        child: Text(
                          "Save",
                          style: TextStyle(color: Colors.white),
                        ),
                        color: const Color(0xFF1BC0C5),
                      ),
                    ),
                    SizedBox(
                      width: 160.0,
                      child: RaisedButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          "Cancel",
                          style: TextStyle(color: Colors.white),
                        ),
                        color: const Color(0xFFFFC0C5),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
