import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:grpc/grpc_web.dart';
import 'package:grpc_web_ks/notification_data.dart';
import 'package:grpc_web_ks/provider/menu_provider.dart';
import 'package:grpc_web_ks/service.dart';
import 'package:provider/provider.dart';
import 'package:grpc_web_ks/global.dart';
import '../gRPC/AccWatch.pbgrpc.dart';
import '../screens/AlertMessage.dart';

class TabSSLCert extends StatefulWidget {
  TabSSLCert({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _TabSSLCertState createState() => _TabSSLCertState();
}

class _TabSSLCertState extends State<TabSSLCert> {
  String _message = "nothing received yet";

  DomainCertificateList nodeResult;
  MenuProvider _menuProvider;

  @override
  void initState() {
    super.initState();
    _menuProvider =  Provider.of<MenuProvider>(context,listen:false);
    getCertList();
  }

  Future<DomainCertificateList> getCertList() async {
        _menuProvider.isLoading = true;
    nodeResult = await DataService().getDomainCertificateList(context);
        _menuProvider.isLoading = false;
    setState(() {});
  }

  Future<MsgReply> domainCertificateSet(DomainCertificate value) async {
    final channel =
        GrpcWebClientChannel.xhr(Uri.parse(apiURL));
    final client = AccWatchAPIClient(channel);
    MsgReply result = await client.domainCertificateSet(DomainCertificate(
        domainCertificateID: value.domainCertificateID,
        domain: value.domain,
        monitor: value.monitor));
    if (result.status == MsgReply_Status.ok) {
      getCertList();
    }
    else
        {
            showDialog(
                  context: context,
                  builder: (BuildContext dialogContext) {
                    return AlertMessage(title: "Cert set failed", content: result.message);
                  },
                );
        }
  }

    Future<MsgReply> domainCertificateDelete(int value) async {
    final channel =
        GrpcWebClientChannel.xhr(Uri.parse(apiURL));
    final client = AccWatchAPIClient(channel);
    MsgReply result = await client.domainCertificateDelete(ID32(iD: value));
    if (result.status == MsgReply_Status.ok) {
      getCertList();
    }
    else
        {
            showDialog(
                  context: context,
                  builder: (BuildContext dialogContext) {
                    return AlertMessage(title: "Delete failed", content: result.message);
                  },
                );
        }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Padding(
        padding: const EdgeInsets.all(10),
        child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Row(
                children: [
                  ...sslCertTitle
                      .asMap()
                      .map((key, value) => MapEntry(
                          key,
                          Container(
                            decoration: boxDecoration(0),
                            height: 30,
                            width: 150,
                            alignment: Alignment.center,
                            child: Text(
                              value,
                              style: TextStyle(fontWeight: FontWeight.w800),
                            ),
                          )))
                      .values
                      .toList()
                ],
              ),
              Column(
                children: [
                  if (nodeResult != null)
                    ...nodeResult.domainCertificate
                        .asMap()
                        .map((key, value) {
                          return MapEntry(
                              key,
                              InkWell(
                                onTap: () async {
                                  var result=await showEditBox(context,value);
                                  if(result!=null){
                                    if(result['remove'])
                                      domainCertificateDelete(value.domainCertificateID);
                                    else
                                      domainCertificateSet(result['node']);
                                  }
                                },
                                highlightColor: Colors.red[100],
                                hoverColor: Colors.red[50],
                                child: Row(
                                  children: [
                                    wdContent(value, 0, value.domain.toString()),
                                    wdContent(
                                        value, 1, "@Stuart#8650"),
                                    wdCheckContent(2, value.monitor.toString()),
                                  ],
                                ),
                              ));
                        })
                        .values
                        .toList(),
                  if (nodeResult == null)
                    ...nodes
                        .asMap()
                        .map((key, value) {
                          return MapEntry(
                              key,
                              Row(
                                children: [
                                  wdContentEmpty(),
                                  wdContentEmpty(),
                                  wdContentEmpty(),
                                ],
                              ));
                        })
                        .values
                        .toList()
                ],
              ),
            ],
          ),
        )),
      ),
      _menuProvider.isLoading?
          SpinKitSpinningLines(color: Colors.blue[400]):Container()
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () async {
          var result = await showEditBox(context, DomainCertificate(),isNew:true);
          if (result != null) {
            domainCertificateSet(result['node']);
          }
        },
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  BoxDecoration boxDecoration(int position) {
    return BoxDecoration(
        color: position == 0 ? Colors.blueGrey.shade100 : Colors.white,
        border: Border(
            top: position == 1
                ? BorderSide.none
                : BorderSide(color: Colors.grey),
            right: BorderSide(color: Colors.grey),
            bottom: BorderSide(color: Colors.grey),
            left: BorderSide(color: Colors.grey)));
  }

  Widget wdContent(DomainCertificate node, int type, String value) {
    return GestureDetector(
      child: Container(
        decoration: boxDecoration(type),
        width: 150,
        height: 25,
        alignment: Alignment.center,
        padding: const EdgeInsets.only(left: 5),
        child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Text(value),
        ),
      ),
    );
  }

  Widget wdContentEmpty() {
    return GestureDetector(
      child: Container(
        decoration: boxDecoration(1),
        width: 150,
        height: 25,
        alignment: Alignment.center,
        padding: const EdgeInsets.only(left: 5),
      ),
      onTap: () async {},
    );
  }

  Widget wdCheckContent(int type, String value) {
    return GestureDetector(
      child: Container(
        decoration: boxDecoration(type),
        width: 150,
        height: 25,
        alignment: Alignment.center,
        child: Icon(value == "true"
            ? Icons.check_box_outlined
            : Icons.check_box_outline_blank),
      ),
    );
  }

  showEditBox(BuildContext context, DomainCertificate node,{bool isNew=false}) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return EditDialog(node: node,isNew: isNew);
        });
  }
}

class EditDialog extends StatefulWidget {
  const EditDialog({this.node, this.isNew, Key key}) : super(key: key);
  final DomainCertificate node;
  final bool isNew;

  @override
  State<EditDialog> createState() => _EditDialogState();
}

class _EditDialogState extends State<EditDialog> {
  String tmpName;
  bool tmpMonitor;
  int tmpCall;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    tmpName = widget.node.domain;
    tmpMonitor = widget.node.monitor;
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20.0)), //this right here
      child: Container(
        height: 400,
        width: 600,
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    child: Text("Domain"),
                  ),
                  Container(
                      width: 350,
                      child: TextFormField(
                        initialValue: tmpName.toString(),
                        keyboardType: TextInputType.text,
                        onChanged: (value) {
                          widget.node.domain = value;
                        },
                      ))
                ],
              ),
              SizedBox(height: 10),
              Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,children: [
                      Container(child: Text("Monitor"),),
                      InkWell(
                        onTap: () {
                          setState(() {
                            tmpMonitor=!tmpMonitor;
                            widget.node.monitor=tmpMonitor;
                          });
                        },
                        child: Container(width: 350,child: Icon(tmpMonitor?Icons.check_box_outlined:Icons.check_box_outline_blank)),
                      )
                    ],),
              Padding(
                padding: EdgeInsets.fromLTRB(30, 20, 30, 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    SizedBox(
                      width: 160.0,
                      child: RaisedButton(
                        onPressed: widget.isNew?null:() {
                          Navigator.of(context).pop({"node": widget.node,"remove":true});
                        },
                        child: Icon(Icons.delete_forever),
                        color: Colors.red,
                      ),
                    ),
                    SizedBox(
                      width: 160.0,
                      child: RaisedButton(
                        onPressed: () {
                          Navigator.of(context).pop({"node": widget.node});
                        },
                        child: Text(
                          "Save",
                          style: TextStyle(color: Colors.white),
                        ),
                        color: const Color(0xFF1BC0C5),
                      ),
                    ),
                    SizedBox(
                      width: 160.0,
                      child: RaisedButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          "Cancel",
                          style: TextStyle(color: Colors.white),
                        ),
                        color: const Color(0xFFFFC0C5),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
