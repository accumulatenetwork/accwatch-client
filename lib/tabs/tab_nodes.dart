import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:grpc/grpc_web.dart';
import 'package:grpc_web_ks/provider/data_provider.dart';
import 'package:grpc_web_ks/provider/menu_provider.dart';
import 'package:grpc_web_ks/service.dart';
import 'package:grpc_web_ks/tabs/tab_networks.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:grpc_web_ks/global.dart';
import '../gRPC/AccWatch.pbgrpc.dart';
import 'package:grpc_web_ks/notification_data.dart';
import 'package:grpc_web_ks/widget/wd_content.dart';
import '../screens/AlertMessage.dart';

class TabNodes extends StatefulWidget {
  TabNodes({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _TabNodesState createState() => _TabNodesState();
}

class _TabNodesState extends State<TabNodes> {
  String _message = "nothing received yet";

  NodeList nodeResult;
  NodeGroupList nodeGroupList;
  NetworkList networkList;

  DataProvider _dataProvider;
  MenuProvider _menuProvider;
  ResponseStream<NodeStatus> result;
  ResponseStream<NetworkStatus> resultNetwork;
  List<NodeStatus> nodeStatusList=[];
  List<NetworkStatus> networkStatusList=[];

  @override
  void initState() {
    super.initState();
    _dataProvider = Provider.of<DataProvider>(context, listen: false);
    _menuProvider = Provider.of<MenuProvider>(context, listen: false);
    getReady();
    getNodeStatus();
    getNetworkStatus();
    // nodeStatusStream();
  }

  getReady() async {
    getNodeGroupList();
    updateNetworkList();
    getNodeList();
  }

  getNodeStatus() async {
    final channel =
        GrpcWebClientChannel.xhr(Uri.parse(apiURL));
    final client = AccWatchAPIClient(channel);
    final prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString("AccessToken");
    result = client.nodeStatusStream(StreamRequest(milliseconds: 1000),
        options:
            CallOptions(metadata: {"Authorization": "Bearer $accessToken"}));
    setState(() {});
  }

  getNetworkStatus() async {
    final channel =
        GrpcWebClientChannel.xhr(Uri.parse(apiURL));
    final client = AccWatchAPIClient(channel);
    final prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString("AccessToken");
    resultNetwork = client.networkStatusStream(StreamRequest(milliseconds: 1000),
        options:
            CallOptions(metadata: {"Authorization": "Bearer $accessToken"}));
    setState(() {});
  }

  getNodeGroupList({bool isRefresh = false}) async {
    if (isRefresh || (!isRefresh && _dataProvider.nodeGroupList == null)) {
      NodeGroupList result = await DataService().getNodeGroupList(context);
      _dataProvider.nodeGroupList = result;
      setState(() {});
    }
  }

  getNodeList({bool isRefresh = false}) async {
    _menuProvider.isLoading = true;
    if (isRefresh || (!isRefresh && _dataProvider.nodeList == null)) {
      NodeList result = await DataService().nodeListGet(context);
      _dataProvider.nodeList = result;
      setState(() {_menuProvider.isLoading = false;});
    }else{
      setState(() {
          _menuProvider.isLoading = false;
      });
    }
  }

  updateNetworkList({bool isRefresh = false}) async {
    if (await _dataProvider.updateNetworkList(context,isRefresh: isRefresh)) {
      setState(() {});
    }
  }

  nodeSet(Node value) async {
    MsgReply result = await DataService().nodeSet(context,value);
    if (result.status == MsgReply_Status.ok) {
      getNodeList(isRefresh: true);
    }
    else
        {
            showDialog(
                  context: context,
                  builder: (BuildContext dialogContext) {
                    return AlertMessage(title: "Set Node failed", content: result.message);
                  },
                );
        }
  }

  nodeDelete(int value) async {
    MsgReply result = await DataService().nodeDelete(context,ID32(iD: value));
    if (result.status == MsgReply_Status.ok) {
      getNodeList(isRefresh: true);
    }
    else
        {
            showDialog(
                  context: context,
                  builder: (BuildContext dialogContext) {
                    return AlertMessage(title: "Delete failed", content: result.message);
                  },
                );
        }
  }

  networkSet(Network value) async {
    MsgReply result = await DataService().networkSet(context,value);
    if (result.status == MsgReply_Status.ok) {
      updateNetworkList(isRefresh: true);
    }
  }

  networkDelete(int value) async {
    MsgReply result = await DataService().networkDelete(context,ID32(iD: value));
    if (result.status == MsgReply_Status.ok) {
      updateNetworkList(isRefresh: true);
    }
    else
        {
            showDialog(
                  context: context,
                  builder: (BuildContext dialogContext) {
                    return AlertMessage(title: "Delete failed", content: result.message);
                  },
                );
        }
  }

  nodeStatusStream() async {
    Stream result = await DataService().nodeStatusStream(context);
  }

  String getNodeGroupText(int nodeGroupID) {
    String text = "";
    if (nodeGroupList != null)
      for (var i = 0; i < nodeGroupList.nodeGroup.length; i++) {
        if (nodeGroupList.nodeGroup[i].nodeGroupID == nodeGroupID) {
          text = nodeGroupList.nodeGroup[i].name;
          break;
        }
      }
    return text;
  }

  @override
  Widget build(BuildContext context) {
    nodeResult = _dataProvider.nodeList;
    nodeGroupList = _dataProvider.nodeGroupList;
    networkList = _dataProvider.networkList;
    return Scaffold(
      body: Stack(
        children: [
          Column(
        children: [
          Container(
            height: (MediaQuery.of(context).size.height - 100) / 2,
            padding: const EdgeInsets.all(10),
            child: Stack(
              children: [
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        wdMainTitle("Nodes"),
                        Row(
                          children: [
                            ...nodeTitle
                                .asMap()
                                .map((key, value) => MapEntry(
                                    key,
                                    Container(
                                      decoration: boxDecoration(0),
                                      height: 30,
                                      width: 150,
                                      alignment: Alignment.center,
                                      child: Text(
                                        value,
                                        style: TextStyle(
                                            fontWeight: FontWeight.w800),
                                      ),
                                    )))
                                .values
                                .toList()
                          ],
                        ),
                        Row(
                          children: [
                            Column(
                              children: [
                                if (nodeResult != null)
                                  ...nodeResult.nodes
                                      .asMap()
                                      .map((key, value) {
                                        return MapEntry(
                                            key,
                                            InkWell(
                                              onTap: () async {
                                                var result = await showEditBox(
                                                    context, value);
                                                if (result != null) {
                                                  if (result['remove'] != null)
                                                    nodeDelete(
                                                        value.nodeID);
                                                  else
                                                    nodeSet(result['node']);
                                                }
                                              },
                                              highlightColor: Colors.red[100],
                                              hoverColor: Colors.red[50],
                                              child: Row(
                                                children: [
                                                  wdContent(
                                                      0,
                                                      getNodeGroupText(
                                                          value.nodeGroupID)),
                                                  wdContent(
                                                      1, value.name.toString()),
                                                  wdContent(
                                                      2, value.host.toString()),
                                                  wdCheckContent(3,
                                                      value.monitor.toString()),
                                                ],
                                              ),
                                            ));
                                      })
                                      .values
                                      .toList(),
                                if (nodeResult == null)
                                  ...nodes
                                      .asMap()
                                      .map((key, value) {
                                        return MapEntry(
                                            key,
                                            Row(
                                              children: [
                                                wdContentEmpty(),
                                                wdContentEmpty(),
                                                wdContentEmpty(),
                                                wdContentEmpty(),
                                                wdContentEmpty(),
                                                wdContentEmpty(),
                                              ],
                                            ));
                                      })
                                      .values
                                      .toList(),
                              ],
                            ),
                            Container(
                              width: 450,
                              child: StreamBuilder<NodeStatus>(
                                stream: result,
                                builder: (
                                  BuildContext context,
                                  AsyncSnapshot<NodeStatus> snapshot,
                                ) {
                                  if (snapshot.connectionState ==
                                      ConnectionState.waiting) {
                                    return Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        CircularProgressIndicator(),
                                        Visibility(
                                          visible: snapshot.hasData,
                                          child: Text(
                                            snapshot.data.toString(),
                                            style: const TextStyle(
                                                color: Colors.black,
                                                fontSize: 24),
                                          ),
                                        ),
                                      ],
                                    );
                                  } else if (snapshot.connectionState ==
                                          ConnectionState.active ||
                                      snapshot.connectionState ==
                                          ConnectionState.done) {
                                    if (snapshot.hasError) {
                                      return const Text('Error');
                                    } else if (snapshot.hasData) {
                                      final index = nodeStatusList.indexWhere((ele)=>ele.nodeID==snapshot.data.nodeID);
                                      if(index>=0){
                                        nodeStatusList[index] = snapshot.data;
                                      }else{
                                        nodeStatusList.add(snapshot.data);
                                      }
                                      return Column(
                                        children: [
                                          if (nodeResult != null)
                                            ...nodeResult.nodes
                                                .asMap()
                                                .map((key, value) {
                                                  final found = nodeStatusList.indexWhere((ele)=>ele.nodeID==value.nodeID);
                                                  if(found>=0)
                                                  return MapEntry(key, Row(
                                                    children: [
                                                      wdContent(
                                                          0,
                                                          nodeStatusList[found].version),
                                                      wdContent(
                                                          1,
                                                          nodeStatusList[found].height.toString()),
                                                      wdContent(
                                                          2,
                                                          nodeStatusList[found].ping.toString()),
                                                    ],
                                                  ));
                                                  return MapEntry(key,Row(
                                                    children: [
                                                      wdContentEmpty(),
                                                      wdContentEmpty(),
                                                      wdContentEmpty()
                                                    ],
                                                  ),);
                                                })
                                                .values
                                                .toList(),
                                          if (nodeResult == null)
                                            ...nodes
                                                .asMap()
                                                .map((key, value) {
                                                  return MapEntry(
                                                      key,
                                                      Row(
                                                        children: [
                                                          wdContentEmpty(),
                                                          wdContentEmpty(),
                                                          wdContentEmpty(),
                                                          wdContentEmpty(),
                                                          wdContentEmpty(),
                                                          wdContentEmpty(),
                                                        ],
                                                      ));
                                                })
                                                .values
                                                .toList(),
                                        ],
                                      );
                                    } else {
                                      return const Text('Empty data');
                                    }
                                  } else {
                                    return Text(
                                        'State: ${snapshot.connectionState}');
                                  }
                                },
                              ),
                            )
                          ],
                        )
                      ],
                    ),
                  ),
                ),
                wdPlusBtn(
                  () async {
                    var result =
                        await showEditBox(context, Node(), isNew: true);
                    if (result != null) {
                      nodeSet(result['node']);
                    }
                  },
                ),
              ],
            ),
          ),
          Container(
            height: 5,
            width: double.infinity,
            color: Colors.blue[500],
          ),
          Container(
            height: (MediaQuery.of(context).size.height - 100) / 2,
            padding: const EdgeInsets.all(10),
            child: Stack(
              children: [
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        wdMainTitle("Networks"),
                        Row(
                          children: [
                            ...networkTitle
                                .asMap()
                                .map((key, value) => MapEntry(
                                    key,
                                    Container(
                                      decoration: boxDecoration(0),
                                      height: 30,
                                      width: 150,
                                      alignment: Alignment.center,
                                      child: Text(
                                        value,
                                        style: TextStyle(
                                            fontWeight: FontWeight.w800),
                                      ),
                                    )))
                                .values
                                .toList()
                          ],
                        ),
                        Row(
                          children: [
                            Column(
                          children: [
                            if (networkList != null)
                              ...networkList.network
                                  .asMap()
                                  .map((key, value) {
                                    return MapEntry(
                                        key,
                                        InkWell(
                                          onTap: () async {
                                            var result = await showEditNetwork(
                                                context, value);
                                            if (result != null) {
                                              if (result['remove'] != null)
                                                networkDelete(value.networkID);
                                              else
                                                networkSet(result['node']);
                                            }
                                          },
                                          highlightColor: Colors.red[100],
                                              hoverColor: Colors.red[50],
                                          child: Row(
                                            children: [
                                              wdContent(0, value.name),
                                              wdContent(1,
                                                  value.blockTime.toString()),
                                              wdContent(
                                                  2,
                                                  value.stalledAfter
                                                      .toString()),
                                              wdContent(
                                                  3,
                                                  value.notifictionID
                                                      .toString()),
                                            ],
                                          ),
                                        ));
                                  })
                                  .values
                                  .toList(),
                            if (nodeResult == null)
                              ...nodes
                                  .asMap()
                                  .map((key, value) {
                                    return MapEntry(
                                        key,
                                        Row(
                                          children: [
                                            wdContentEmpty(),
                                            wdContentEmpty(),
                                            wdContentEmpty(),
                                            wdContentEmpty(),
                                          ],
                                        ));
                                  })
                                  .values
                                  .toList()
                          ],
                        ),
                        Container(
                              width: 300,
                              child: StreamBuilder<NetworkStatus>(
                                stream: resultNetwork,
                                builder: (
                                  BuildContext context,
                                  AsyncSnapshot<NetworkStatus> snapshot,
                                ) {
                                  if (snapshot.connectionState ==
                                      ConnectionState.waiting) {
                                    return Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        CircularProgressIndicator(),
                                        Visibility(
                                          visible: snapshot.hasData,
                                          child: Text(
                                            snapshot.data.toString(),
                                            style: const TextStyle(
                                                color: Colors.black,
                                                fontSize: 24),
                                          ),
                                        ),
                                      ],
                                    );
                                  } else if (snapshot.connectionState ==
                                          ConnectionState.active ||
                                      snapshot.connectionState ==
                                          ConnectionState.done) {
                                    if (snapshot.hasError) {
                                      return const Text('Error');
                                    } else if (snapshot.hasData) {
                                      print(["network result:",snapshot.data]);
                                      final index = networkStatusList.indexWhere((ele)=>ele.networkID==snapshot.data.networkID);
                                      if(index>=0){
                                        networkStatusList[index] = snapshot.data;
                                      }else{
                                        networkStatusList.add(snapshot.data);
                                      }
                                      return Column(
                                        children: [
                                          if (networkList != null)
                                            ...networkList.network
                                                .asMap()
                                                .map((key, value) {
                                                  final found = networkStatusList.indexWhere((ele)=>ele.networkID==value.networkID);
                                                  if(found>=0)
                                                    return MapEntry(key, Row(
                                                      children: [
                                                        wdContent(
                                                            1,
                                                            networkStatusList[found].height.toString()),
                                                        wdContent(
                                                            2,
                                                            networkStatusList[found].averageTime.toString()),
                                                      ],
                                                    ));
                                                  return MapEntry(key,Row(
                                                    children: [
                                                      wdContentEmpty(),
                                                      wdContentEmpty()
                                                    ],
                                                  ),);
                                                })
                                                .values
                                                .toList(),
                                          if (nodeResult == null)
                                            ...nodes
                                                .asMap()
                                                .map((key, value) {
                                                  return MapEntry(
                                                      key,
                                                      Row(
                                                        children: [
                                                          wdContentEmpty(),
                                                          wdContentEmpty(),
                                                          wdContentEmpty(),
                                                          wdContentEmpty(),
                                                          wdContentEmpty(),
                                                          wdContentEmpty(),
                                                        ],
                                                      ));
                                                })
                                                .values
                                                .toList(),
                                        ],
                                      );
                                    } else {
                                      return const Text('Empty data');
                                    }
                                  } else {
                                    return Text(
                                        'State: ${snapshot.connectionState}');
                                  }
                                },
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
                wdPlusBtn(() async {
                  var result =
                      await showEditNetwork(context, Network(), isNew: true);
                  if (result != null) {
                    networkSet(result['node']);
                  }
                }),
              ],
            ),
          ),
        ],
      ),
      _menuProvider.isLoading?
          SpinKitSpinningLines(color: Colors.blue[400]):Container()
        ],
      ),
    );
  }


  showEditBox(BuildContext context, Node node, {bool isNew = false}) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return EditNodeDialog(node: node);
        });
  }

  showEditNetwork(BuildContext context, Network node, {bool isNew = false}) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return EditNetworkDialog(node: node, isNew: isNew);
        });
  }
}

class EditNodeDialog extends StatefulWidget {
  const EditNodeDialog({this.node, this.isNew = false, Key key})
      : super(key: key);
  final Node node;
  final bool isNew;

  @override
  State<EditNodeDialog> createState() => _EditNodeDialogState();
}

class _EditNodeDialogState extends State<EditNodeDialog> {
  bool tmpMonitor;
  int tmpGrupID;
  String tmpName;
  String tmpHost;
  NodeGroupList nodeGroupList;
  NodeGroup nodeGroupValue;
  DataProvider _dataProvider;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    tmpMonitor = widget.node.monitor ?? false;
    tmpGrupID = widget.node.nodeGroupID;
    tmpName = widget.node.name;
    tmpHost = widget.node.host;
    _dataProvider = Provider.of<DataProvider>(context, listen: false);
    getReady();
  }

  getReady() async {
    nodeGroupList = _dataProvider.nodeGroupList;
    if (tmpGrupID != null) {
      for (var i = 0; i < nodeGroupList.nodeGroup.length; i++) {
        if (nodeGroupList.nodeGroup[i].nodeGroupID == tmpGrupID) {
          nodeGroupValue = nodeGroupList.nodeGroup[i];
        }
      }
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20.0)), //this right here
      child: Container(
        height: 400,
        width: 600,
        child: nodeGroupList == null
            ? Container()
            : Padding(
                padding: const EdgeInsets.all(12.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          child: Text("GroupID"),
                        ),
                        Container(
                            width: 350,
                            child: DropdownButton(
                              value: nodeGroupValue,
                              items:
                                  nodeGroupList.nodeGroup.map((NodeGroup item) {
                                return DropdownMenuItem<NodeGroup>(
                                  child: Text(item.name),
                                  value: item,
                                );
                              }).toList(),
                              onChanged: (NodeGroup value) {
                                setState(() {
                                  nodeGroupValue = value;
                                  widget.node.nodeGroupID = value.nodeGroupID;
                                });
                              },
                              hint: Text("Select item"),
                              disabledHint: Text("Disabled"),
                              elevation: 8,
                              style:
                                  TextStyle(color: Colors.green, fontSize: 16),
                              icon: Icon(Icons.arrow_drop_down_circle),
                              iconDisabledColor: Colors.red,
                              iconEnabledColor: Colors.green,
                              isExpanded: true,
                            ))
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          child: Text("Name"),
                        ),
                        Container(
                            width: 350,
                            child: TextFormField(
                              initialValue: tmpName.toString(),
                              keyboardType: TextInputType.text,
                              onChanged: (value) {
                                widget.node.name = value;
                              },
                            ))
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          child: Text("Host"),
                        ),
                        Container(
                            width: 350,
                            child: TextFormField(
                              initialValue: tmpHost.toString(),
                              keyboardType: TextInputType.numberWithOptions(
                                  decimal: true),
                              inputFormatters: [
                                FilteringTextInputFormatter.allow(
                                    RegExp('[0-9.,]')),
                              ],
                              onChanged: (value) {
                                widget.node.host = value;
                              },
                            ))
                      ],
                    ),
                    SizedBox(height: 10),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          child: Text("Monitor"),
                        ),
                        InkWell(
                          onTap: () {
                            setState(() {
                              tmpMonitor = !tmpMonitor;
                              widget.node.monitor = tmpMonitor;
                            });
                          },
                          child: Container(
                              width: 350,
                              child: Icon(tmpMonitor
                                  ? Icons.check_box_outlined
                                  : Icons.check_box_outline_blank)),
                        )
                      ],
                    ),
                    SizedBox(height: 10),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30, 20, 30, 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          SizedBox(
                            width: 160.0,
                            child: RaisedButton(
                              onPressed: widget.isNew
                                  ? null
                                  : () {
                                      Navigator.of(context).pop({
                                        "node": widget.node,
                                        "remove": true
                                      });
                                    },
                              child: Icon(Icons.delete_forever),
                              color: Colors.red,
                            ),
                          ),
                          SizedBox(
                            width: 160.0,
                            child: RaisedButton(
                              onPressed: () {
                                Navigator.of(context)
                                    .pop({"node": widget.node});
                              },
                              child: Text(
                                "Save",
                                style: TextStyle(color: Colors.white),
                              ),
                              color: const Color(0xFF1BC0C5),
                            ),
                          ),
                          SizedBox(
                            width: 160.0,
                            child: RaisedButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              child: Text(
                                "Cancel",
                                style: TextStyle(color: Colors.white),
                              ),
                              color: const Color(0xFFFFC0C5),
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
      ),
    );
  }
}
