import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:grpc_web_ks/notification_data.dart';
import 'package:grpc_web_ks/provider/data_provider.dart';
import 'package:grpc_web_ks/provider/menu_provider.dart';
import 'package:grpc_web_ks/service.dart';
import 'package:grpc_web_ks/tabs/tab_notification_policy.dart';
import 'package:grpc_web_ks/widget/wd_content.dart';
import 'package:provider/provider.dart';
import '../gRPC/AccWatch.pbgrpc.dart';
import '../screens/AlertMessage.dart';

class TabNodeGroups extends StatefulWidget {
  TabNodeGroups({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _TabNodeGroupsState createState() => _TabNodeGroupsState();
}

class _TabNodeGroupsState extends State<TabNodeGroups> {
  String _message = "nothing received yet";

  NodeGroupList nodeGroupList;
  NetworkList networkList;
  NotificationPolicyList notificationPolicyList;
  DataProvider _dataProvider;
  MenuProvider _menuProvider;

  @override
  void initState() {
    super.initState();
    _dataProvider = Provider.of<DataProvider>(context, listen: false);
    _menuProvider = Provider.of<MenuProvider>(context, listen: false);
    getReady();
  }

  getReady() async {
    _menuProvider.isLoading = true;
    await updateNetworkList();
    await updateNotificationPolicyList();
    await updateNodeGroupList();
    setState(() {
          _menuProvider.isLoading = false;
      });
  }

  updateNodeGroupList({bool isRefresh = false}) async {
    _menuProvider.isLoading = true;
    if (await _dataProvider.updateNodeGroupList(context,isRefresh: isRefresh)) {
      setState(() {_menuProvider.isLoading = false;});
    }else{
      setState(() {
          _menuProvider.isLoading = false;
      });
    }
  }

  Future<bool> updateNetworkList({bool isRefresh = false}) async {
    if (await _dataProvider.updateNetworkList(context,isRefresh: isRefresh)) {
      setState(() {});
    }
    return true;
  }

  updateNotificationPolicyList({bool isRefresh = false}) async {
    if (await _dataProvider.updateNotificationPolicyList(context,
        isRefresh: isRefresh)) {
      setState(() {});
    }
  }

  nodeGroupSet(NodeGroup value) async {
    MsgReply result = await DataService().nodeGroupSet(context,value);
    if (result.status == MsgReply_Status.ok) {
      updateNodeGroupList(isRefresh: true);
    }
  }

  nodeGroupDelete(int value) async {
    MsgReply result = await DataService().nodeGroupDelete(context,ID32(iD: value));
    if (result.status == MsgReply_Status.ok)
    {
        updateNodeGroupList(isRefresh: true);
    }
    else
    {
        showDialog(
              context: context,
              builder: (BuildContext dialogContext) {
                return AlertMessage(title: "Delete failed", content: result.message);
              },
            );
    }
  }

  Future<bool> notificationPolicySet(NotificationPolicy value) async {
    MsgReply result = await DataService().notificationPolicySet(context,value);
    if (result.status == MsgReply_Status.ok) {
      updateNotificationPolicyList(isRefresh: true);
    }
    return true;
  }

  Future<bool> notificationPolicyDelete(int value) async {
    MsgReply result =
        await DataService().notificationPolicyDelete(context,ID32(iD: value));
    if (result.status == MsgReply_Status.ok) {
      updateNotificationPolicyList(isRefresh: true);
    }
    else
        {
            showDialog(
                  context: context,
                  builder: (BuildContext dialogContext) {
                    return AlertMessage(title: "Delete failed", content: result.message);
                  },
                );
        }
    return true;
  }

  @override
  Widget build(BuildContext context) {
    nodeGroupList = _dataProvider.nodeGroupList;
    networkList = _dataProvider.networkList;
    notificationPolicyList = _dataProvider.notificationPolicyList;

    return Scaffold(
      body: Stack(
        children: [
          Column(
        children: [
          Container(
            height: (MediaQuery.of(context).size.height - 100) / 2,
            padding: const EdgeInsets.all(10),
            child: Stack(
              children: [
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: SingleChildScrollView(
                  scrollDirection: Axis.vertical,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      wdMainTitle("Node Groups"),
                      Row(
                        children: [
                          Container(
                              width: 300,
                              height: 30,
                              decoration: BoxDecoration(
                                  border: Border(
                                top: BorderSide(color: Colors.grey),
                                left: BorderSide(color: Colors.grey),
                              ))),
                          Container(
                              alignment: Alignment.center,
                              child: Text("Notification"),
                              width: 450,
                              height: 30,
                              decoration: BoxDecoration(
                                  color: Colors.redAccent[100],
                                  border: Border(
                                    top: BorderSide(color: Colors.grey),
                                    right: BorderSide(color: Colors.grey),
                                    left: BorderSide(color: Colors.grey),
                                  )))
                        ],
                      ),
                      Row(
                        children: [
                          ...nodeGrupTitle
                              .asMap()
                              .map((key, value) => MapEntry(
                                  key,
                                  Container(
                                    decoration: boxDecoration(0),
                                    height: 30,
                                    width: 150,
                                    alignment: Alignment.center,
                                    child: Text(
                                      value,
                                      style: TextStyle(
                                          fontWeight: FontWeight.w800),
                                    ),
                                  )))
                              .values
                              .toList()
                        ],
                      ),
                      Column(
                        children: [
                          if (nodeGroupList != null &&
                              networkList != null &&
                              notificationPolicyList != null)
                            ...nodeGroupList.nodeGroup
                                .asMap()
                                .map((key, value) {
                                  return MapEntry(
                                      key,
                                      InkWell(
                                        onTap: () async {
                                          var result =
                                              await showEditBox(context, value);
                                          if (result != null) {
                                            if (result['remove'] != null)
                                              nodeGroupDelete(
                                                  value.nodeGroupID);
                                            else
                                              nodeGroupSet(result['node']);
                                          }
                                        },
                                        highlightColor: Colors.red[100],
                                        hoverColor: Colors.red[50],
                                        child: Row(
                                          children: [
                                            wdContent(1, value.name.toString()),
                                            wdContent(
                                                1,
                                                getNetworkText(
                                                    value.networkID)),
                                            wdContent(
                                                2,
                                                getNotificationPolicyText(
                                                    value.pingNotifictionID)),
                                            wdContent(
                                                2,
                                                getNotificationPolicyText(
                                                    value.heightNotifictionID)),
                                            wdContent(
                                                2,
                                                getNotificationPolicyText(value
                                                    .latencyNotifictionID)),
                                          ],
                                        ),
                                      ));
                                })
                                .values
                                .toList(),
                          if (nodeGroupList == null)
                            ...nodes
                                .asMap()
                                .map((key, value) {
                                  return MapEntry(
                                      key,
                                      Row(
                                        children: [
                                          wdContentEmpty(),
                                          wdContentEmpty(),
                                          wdContentEmpty(),
                                          wdContentEmpty(),
                                          wdContentEmpty(),
                                        ],
                                      ));
                                })
                                .values
                                .toList()
                        ],
                      ),
                    ],
                  ),
                ),
                ),
                wdPlusBtn(
                  () async {
                    var result =
                        await showEditBox(context, NodeGroup(), isNew: true);
                    if (result != null) {
                      nodeGroupSet(result['node']);
                    }
                  },
                ),
              ],
            ),
          ),
          Container(
            height: 5,
            width: double.infinity,
            color: Colors.blue[500],
          ),
          Container(
            height: (MediaQuery.of(context).size.height - 100) / 2,
            padding: const EdgeInsets.all(10),
            child: Stack(
              children: [
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  wdMainTitle("Notification Timers"),
                  Row(
                    children: [
                      Container(
                          width: 150,
                          height: 30,
                          decoration: BoxDecoration(
                              border: Border(
                            top: BorderSide(color: Colors.grey),
                            left: BorderSide(color: Colors.grey),
                          ))),
                      Container(
                          alignment: Alignment.center,
                          child: Text(
                            "Seconds",
                            style: TextStyle(fontWeight: FontWeight.w800),
                          ),
                          width: 300,
                          height: 30,
                          decoration: BoxDecoration(
                              color: Colors.green,
                              border: Border(
                                top: BorderSide(color: Colors.grey),
                                right: BorderSide(color: Colors.grey),
                                left: BorderSide(color: Colors.grey),
                              )))
                    ],
                  ),
                  Row(
                    children: [
                      ...notificationPoliyTitle
                          .asMap()
                          .map((key, value) => MapEntry(
                              key,
                              Container(
                                decoration: boxDecoration(0),
                                height: 30,
                                width: 150,
                                alignment: Alignment.center,
                                child: Text(
                                  value,
                                  style: TextStyle(fontWeight: FontWeight.w800),
                                ),
                              )))
                          .values
                          .toList()
                    ],
                  ),
                  Column(
                    children: [
                      if (notificationPolicyList != null)
                        ...notificationPolicyList.notificationPolicyList
                            .asMap()
                            .map((key, value) {
                              return MapEntry(
                                  key,
                                  InkWell(
                                    onTap: () async {
                                      var result =
                                          await showEditNotificationPolicy(
                                              context, value);
                                      if (result != null) {
                                        if (result['remove'] != null)
                                          notificationPolicyDelete(
                                              value.notifictionID);
                                        else
                                          notificationPolicySet(result['node']);
                                      }
                                    },
                                    highlightColor: Colors.red[100],
                                    hoverColor: Colors.red[50],
                                    child: Row(
                                      children: [
                                        wdContent(0, value.name.toString()),
                                        wdContent(1, value.discord.toString()),
                                        wdContent(2, value.call.toString()),
                                      ],
                                    ),
                                  ));
                            })
                            .values
                            .toList(),
                      if (notificationPolicyList == null)
                        ...nodes
                            .asMap()
                            .map((key, value) {
                              return MapEntry(
                                  key,
                                  Row(
                                    children: [
                                      wdContentEmpty(),
                                      wdContentEmpty(),
                                      wdContentEmpty(),
                                    ],
                                  ));
                            })
                            .values
                            .toList()
                    ],
                  ),
                ],
              ),
            ),
                ),
            wdPlusBtn(
                  () async {
                    var result =
                        await showEditNotificationPolicy(context, NotificationPolicy(), isNew: true);
                    if (result != null) {
                      notificationPolicySet(result['node']);
                    }
                  },
                ),
              ],
            ),
          ),

        ],
      ),
      _menuProvider.isLoading?
          SpinKitSpinningLines(color: Colors.blue[400]):Container()
        ],
      ),
         );
  }

  showEditBox(BuildContext context, NodeGroup node, {bool isNew = false}) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return EditNodeGroupDialog(node: node, isNew: isNew);
        });
  }

  showEditNotificationPolicy(BuildContext context, NotificationPolicy node,
      {bool isNew = false}) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return EditNotificationPolicyDialog(node: node, isNew: isNew);
        });
  }

  String getNetworkText(int networkID) {
    String text = "";
    for (var i = 0; i < networkList.network.length; i++) {
      if (networkList.network[i].networkID == networkID) {
        text = networkList.network[i].name;
        break;
      }
    }
    return text;
  }

  String getNotificationPolicyText(int notificationID) {
    String text = "";
    for (var i = 0;
        i < notificationPolicyList.notificationPolicyList.length;
        i++) {
      if (notificationPolicyList.notificationPolicyList[i].notifictionID ==
          notificationID) {
        text = notificationPolicyList.notificationPolicyList[i].name;
        break;
      }
    }
    return text;
  }
}
class EditNodeGroupDialog extends StatefulWidget {
  const EditNodeGroupDialog({this.node, this.isNew, Key key}) : super(key: key);
  final NodeGroup node;
  final bool isNew;

  @override
  State<EditNodeGroupDialog> createState() => _EditNodeGroupDialogState();
}
class _EditNodeGroupDialogState extends State<EditNodeGroupDialog> {
  int tmpNetworkId;
  int tmpPingNotificationID;
  int tmpHeightNotificationID;
  int tmpLatencyNotificationID;
  String tmpName;
  Network _networkValue;
  NotificationPolicy _pingValue;
  NotificationPolicy _heightValue;
  NotificationPolicy _latencyValue;
  NetworkList networkList;
  NotificationPolicyList notificationPolicyList;
  DataProvider _dataProvider;
  @override
  void initState() {
    super.initState();
    _dataProvider = Provider.of<DataProvider>(context, listen: false);
    tmpNetworkId = widget.node.networkID;
    tmpName = widget.node.name;
    tmpPingNotificationID = widget.node.pingNotifictionID;
    tmpHeightNotificationID = widget.node.heightNotifictionID;
    tmpLatencyNotificationID = widget.node.latencyNotifictionID;
    getReady();
  }

  getReady() async {
    networkList = _dataProvider.networkList;
    notificationPolicyList = _dataProvider.notificationPolicyList;
    if (tmpNetworkId != null) {
      for (var i = 0; i < networkList.network.length; i++) {
        if (networkList.network[i].networkID == tmpNetworkId) {
          _networkValue = networkList.network[i];
          break;
        }
      }
    }
    if (tmpPingNotificationID != null) {
      for (var i = 0;
          i < notificationPolicyList.notificationPolicyList.length;
          i++) {
        if (notificationPolicyList.notificationPolicyList[i].notifictionID ==
            tmpPingNotificationID) {
          _pingValue = notificationPolicyList.notificationPolicyList[i];
          break;
        }
      }
    }
    if (tmpHeightNotificationID != null) {
      for (var i = 0;
          i < notificationPolicyList.notificationPolicyList.length;
          i++) {
        if (notificationPolicyList.notificationPolicyList[i].notifictionID ==
            tmpHeightNotificationID) {
          _heightValue = notificationPolicyList.notificationPolicyList[i];
          break;
        }
      }
    }
    if (tmpLatencyNotificationID != null) {
      for (var i = 0;
          i < notificationPolicyList.notificationPolicyList.length;
          i++) {
        if (notificationPolicyList.notificationPolicyList[i].notifictionID ==
            tmpLatencyNotificationID) {
          _latencyValue = notificationPolicyList.notificationPolicyList[i];
          break;
        }
      }
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20.0)), //this right here
      child: Container(
        height: 400,
        width: 600,
        child: (networkList == null || notificationPolicyList == null)
            ? Container()
            : Padding(
                padding: const EdgeInsets.all(12.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          child: Text("Name"),
                        ),
                        Container(
                            width: 350,
                            child: TextFormField(
                              initialValue: tmpName.toString(),
                              keyboardType: TextInputType.text,
                              onChanged: (value) {
                                // tmpName = value;
                                widget.node.name = value.toString();
                              },
                            ))
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          child: Text("Network"),
                        ),
                        Container(
                            width: 350,
                            child: DropdownButton(
                              value: _networkValue,
                              items: networkList.network.map((Network item) {
                                return DropdownMenuItem<Network>(
                                  child: Text(item.name),
                                  value: item,
                                );
                              }).toList(),
                              onChanged: (Network value) {
                                setState(() {
                                  _networkValue = value;
                                  widget.node.networkID = value.networkID;
                                });
                              },
                              hint: Text("Select item"),
                              disabledHint: Text("Disabled"),
                              elevation: 8,
                              style:
                                  TextStyle(color: Colors.green, fontSize: 16),
                              icon: Icon(Icons.arrow_drop_down_circle),
                              iconDisabledColor: Colors.red,
                              iconEnabledColor: Colors.green,
                              isExpanded: true,
                            ))
                      ],
                    ),
                    SizedBox(height: 10),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          child: Text("PingNetwork"),
                        ),
                        Container(
                            width: 350,
                            child: DropdownButton(
                              value: _pingValue,
                              items: notificationPolicyList
                                  .notificationPolicyList
                                  .map((NotificationPolicy item) {
                                return DropdownMenuItem<NotificationPolicy>(
                                  child: Text(item.name),
                                  value: item,
                                );
                              }).toList(),
                              onChanged: (value) {
                                setState(() {
                                  _pingValue = value;
                                  widget.node.pingNotifictionID =
                                      value.notifictionID;
                                });
                              },
                              hint: Text("Select item"),
                              disabledHint: Text("Disabled"),
                              elevation: 8,
                              style:
                                  TextStyle(color: Colors.green, fontSize: 16),
                              icon: Icon(Icons.arrow_drop_down_circle),
                              iconDisabledColor: Colors.red,
                              iconEnabledColor: Colors.green,
                              isExpanded: true,
                            ))
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          child: Text("HeightNotification"),
                        ),
                        Container(
                            width: 350,
                            child: DropdownButton(
                              value: _heightValue,
                              items: notificationPolicyList
                                  .notificationPolicyList
                                  .map((NotificationPolicy item) {
                                return DropdownMenuItem<NotificationPolicy>(
                                  child: Text(item.name),
                                  value: item,
                                );
                              }).toList(),
                              onChanged: (value) {
                                setState(() {
                                  _heightValue = value;
                                  widget.node.heightNotifictionID =
                                      value.notifictionID;
                                });
                              },
                              hint: Text("Select item"),
                              disabledHint: Text("Disabled"),
                              elevation: 8,
                              style:
                                  TextStyle(color: Colors.green, fontSize: 16),
                              icon: Icon(Icons.arrow_drop_down_circle),
                              iconDisabledColor: Colors.red,
                              iconEnabledColor: Colors.green,
                              isExpanded: true,
                            ))
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Container(
                          child: Text("LatencyNotification"),
                        ),
                        Container(
                            width: 350,
                            child: DropdownButton(
                              value: _latencyValue,
                              items: notificationPolicyList
                                  .notificationPolicyList
                                  .map((NotificationPolicy item) {
                                return DropdownMenuItem<NotificationPolicy>(
                                  child: Text(item.name),
                                  value: item,
                                );
                              }).toList(),
                              onChanged: (NotificationPolicy value) {
                                setState(() {
                                  _latencyValue = value;
                                  widget.node.latencyNotifictionID =
                                      value.notifictionID;
                                });
                              },
                              hint: Text("Select item"),
                              disabledHint: Text("Disabled"),
                              elevation: 8,
                              style:
                                  TextStyle(color: Colors.green, fontSize: 16),
                              icon: Icon(Icons.arrow_drop_down_circle),
                              iconDisabledColor: Colors.red,
                              iconEnabledColor: Colors.green,
                              isExpanded: true,
                            ))
                      ],
                    ),
                    SizedBox(height: 10),
                    Padding(
                      padding: EdgeInsets.fromLTRB(30, 20, 30, 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          SizedBox(
                            width: 160.0,
                            child: RaisedButton(
                              onPressed: widget.isNew
                                  ? null
                                  : () {
                                      Navigator.of(context).pop({
                                        "node": widget.node,
                                        "remove": true
                                      });
                                    },
                              child: Icon(Icons.delete_forever),
                              color: Colors.red,
                            ),
                          ),
                          SizedBox(
                            width: 160.0,
                            child: RaisedButton(
                              onPressed: () {
                                Navigator.of(context)
                                    .pop({"node": widget.node});
                              },
                              child: Text(
                                "Save",
                                style: TextStyle(color: Colors.white),
                              ),
                              color: const Color(0xFF1BC0C5),
                            ),
                          ),
                          SizedBox(
                            width: 160.0,
                            child: RaisedButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              child: Text(
                                "Cancel",
                                style: TextStyle(color: Colors.white),
                              ),
                              color: const Color(0xFFFFC0C5),
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
              ),
      ),
    );
  }
}
