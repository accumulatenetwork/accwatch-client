import 'package:flutter/material.dart';
import '../gRPC/AccWatch.pbgrpc.dart';
import 'package:flutter/services.dart';
import '../screens/AlertMessage.dart';

class EditNotificationPolicyDialog extends StatefulWidget {
  const EditNotificationPolicyDialog({this.node, this.isNew, Key key}) : super(key: key);
  final NotificationPolicy node;
  final bool isNew;

  @override
  State<EditNotificationPolicyDialog> createState() => _EditNotificationPolicyDialogState();
}

class _EditNotificationPolicyDialogState extends State<EditNotificationPolicyDialog> {
  String tmpName;
  int tmpDiscord;
  int tmpCall;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    tmpName = widget.node.name;
    tmpDiscord = widget.node.discord;
    tmpCall = widget.node.call;
  }

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20.0)), //this right here
      child: Container(
        height: 400,
        width: 600,
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    child: Text("Name"),
                  ),
                  Container(
                      width: 350,
                      child: TextFormField(
                        initialValue: tmpName.toString(),
                        keyboardType: TextInputType.text,
                        onChanged: (value) {
                          widget.node.name = value;
                        },
                      ))
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    child: Text("Discord"),
                  ),
                  Container(
                      width: 350,
                      child: TextFormField(
                        initialValue: tmpDiscord.toString(),
                        keyboardType: TextInputType.number,
                        inputFormatters: [FilteringTextInputFormatter.allow(RegExp('[0-9.,-]')),],
                        onChanged: (value) {
                          if (value != "")
                            widget.node.discord = int.parse(value);
                        },
                      ))
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    child: Text("Call"),
                  ),
                  Container(
                      width: 350,
                      child: TextFormField(
                        initialValue: tmpCall.toString(),
                        keyboardType: TextInputType.number,
                        inputFormatters: [FilteringTextInputFormatter.allow(RegExp('[0-9.,-]')),],
                        onChanged: (value) {
                          if (value != "")
                            widget.node.call = int.parse(value);
                        },
                      ))
                ],
              ),
              SizedBox(height: 10),
              Padding(
                padding: EdgeInsets.fromLTRB(30, 20, 30, 10),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    SizedBox(
                      width: 160.0,
                      child: RaisedButton(
                        onPressed: widget.isNew?null:() {
                          Navigator.of(context).pop({"node": widget.node,"remove":true});
                        },
                        child: Icon(Icons.delete_forever),
                        color: Colors.red,
                      ),
                    ),
                    SizedBox(
                      width: 160.0,
                      child: RaisedButton(
                        onPressed: () {
                          Navigator.of(context).pop({"node": widget.node});
                        },
                        child: Text(
                          "Save",
                          style: TextStyle(color: Colors.white),
                        ),
                        color: const Color(0xFF1BC0C5),
                      ),
                    ),
                    SizedBox(
                      width: 160.0,
                      child: RaisedButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: Text(
                          "Cancel",
                          style: TextStyle(color: Colors.white),
                        ),
                        color: const Color(0xFFFFC0C5),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
