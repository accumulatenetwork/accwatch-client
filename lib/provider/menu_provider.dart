import 'package:flutter/material.dart';

class MenuProvider extends ChangeNotifier{
  int _selectedIndex=0;

  bool _isLoading=false;

  get selectedIndex=>this._selectedIndex;
  set selectedIndex(value){
    this._selectedIndex = value;
  }

  get isLoading=>this._isLoading;
  set isLoading(value){
    this._isLoading = value;
  }
}