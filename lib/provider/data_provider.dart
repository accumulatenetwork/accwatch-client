import 'package:flutter/material.dart';
import 'package:grpc/grpc_web.dart';
import 'package:grpc_web_ks/gRPC/google/protobuf/empty.pb.dart';
import 'package:grpc_web_ks/provider/data_provider.dart';
import 'package:grpc_web_ks/service.dart';
import 'package:provider/provider.dart';

import '../gRPC/AccWatch.pbgrpc.dart';
import '../gRPC/AccWatch.pb.dart';

class DataProvider extends ChangeNotifier{

  NodeGroupList _nodeGroupList;
  NodeList _nodeResult;
  NetworkList _networkList;
  NotificationPolicyList _notificationPolicyList;


  get nodeGroupList=>this._nodeGroupList;
  set nodeGroupList(value){
    this._nodeGroupList = value;
  }
  get nodeList=>this._nodeResult;
  set nodeList(value){
    this._nodeResult = value;
  }

  get networkList=>this._networkList;
  set networkList(value){
    this._networkList = value;
  }

  get notificationPolicyList=>this._notificationPolicyList;
  set notificationPolicyList(value){
    this._notificationPolicyList = value;
  }

  Future<bool> updateNotificationPolicyList(BuildContext context,{bool isRefresh = false})async{
    if (isRefresh || (!isRefresh && _notificationPolicyList == null)) {
      NotificationPolicyList result = await DataService().getNotificationPolicyList(context,);
      _notificationPolicyList = result;
      return true;
    }
    else return false;
  }
  Future<bool> updateNetworkList(BuildContext context,{bool isRefresh = false})async{
    if (isRefresh || (!isRefresh && _networkList == null)) {
      NetworkList result = await DataService().getNetworkList(context);
      _networkList = result;
      return true;
    }
    else return false;
  }

  Future<bool> updateNodeGroupList(BuildContext context,{bool isRefresh = false})async{
    if (isRefresh || (!isRefresh && _nodeGroupList == null)) {
      NodeGroupList result = await DataService().getNodeGroupList(context);
      _nodeGroupList = result;
      return true;
    }
    else return false;
  }
}