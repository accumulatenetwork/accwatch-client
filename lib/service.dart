import 'dart:html';

import 'package:flutter/material.dart';
import 'package:grpc/grpc_web.dart';
import 'package:grpc_web_ks/gRPC/google/protobuf/empty.pb.dart';
import 'package:grpc_web_ks/global.dart';
import 'package:grpc_web_ks/screens/login.dart';

import 'gRPC/AccWatch.pbgrpc.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DataService {
  final channel =
      GrpcWebClientChannel.xhr(Uri.parse(apiURL));

  Future<UserList> userListGet(BuildContext context) async {
    final client = AccWatchAPIClient(channel);
    final prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString("AccessToken");
   
    try{
      UserList result = await client.userListGet(Empty(),
        options:
            CallOptions(metadata: {"Authorization": "Bearer $accessToken"}));
    return result;
   }catch(e){
     print(["wdewef:",e.toString()]);
     goLogin(context);
   }
  }

  Future<MsgReply> userSet(BuildContext context,User value) async {
    final client = AccWatchAPIClient(channel);
    final prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString("AccessToken");
    
    try{
     MsgReply result = await client.userSet(value,
        options:
            CallOptions(metadata: {"Authorization": "Bearer $accessToken"}));
    return result;
   }catch(e){
     print(["wdewef:",e.toString()]);
     goLogin(context);
   }
  }

  Future<MsgReply> userDelete(BuildContext context,ID32 value) async {
    final client = AccWatchAPIClient(channel);
    final prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString("AccessToken");
    
    try{
     MsgReply result = await client.userDelete(value,
        options:
            CallOptions(metadata: {"Authorization": "Bearer $accessToken"}));
    return result;
   }catch(e){
     print(["wdewef:",e.toString()]);
     goLogin(context);
   }
  }    

  Future<NodeList> nodeListGet(BuildContext context) async {
    final client = AccWatchAPIClient(channel);
    final prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString("AccessToken");
    
    try{
     NodeList result = await client.nodeListGet(Empty(),
        options:
            CallOptions(metadata: {"Authorization": "Bearer $accessToken"}));
    return result;
   }catch(e){
     print(["wdewef:",e.toString()]);
     goLogin(context);
   }
  }

  Future<MsgReply> nodeSet(BuildContext context,Node value) async {
    final client = AccWatchAPIClient(channel);
    final prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString("AccessToken");
    
    try{
     MsgReply result = await client.nodeSet(value,
        options:
            CallOptions(metadata: {"Authorization": "Bearer $accessToken"}));
    return result;
   }catch(e){
     print(["wdewef:",e.toString()]);
     goLogin(context);
   }
  }

  Future<MsgReply> nodeDelete(BuildContext context,ID32 value) async {
    final client = AccWatchAPIClient(channel);
    final prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString("AccessToken");
    
    try{
     MsgReply result = await client.nodeDelete(value,
        options:
            CallOptions(metadata: {"Authorization": "Bearer $accessToken"}));
    return result;
   }catch(e){
     print(["wdewef:",e.toString()]);
     goLogin(context);
   }
  }

  Future<NetworkList> getNetworkList(BuildContext context) async {
    final client = AccWatchAPIClient(channel);
    final prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString("AccessToken");
    
    try{
     NetworkList result = await client.networkListGet(Empty(),
        options:
            CallOptions(metadata: {"Authorization": "Bearer $accessToken"}));
    return result;
   }catch(e){
     print(["wdewef:",e.toString()]);
     goLogin(context);
   }
  }

  Future<MsgReply> networkSet(BuildContext context,Network value) async {
    final client = AccWatchAPIClient(channel);
    final prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString("AccessToken");
    
    try{
     MsgReply result = await client.networkSet(value,
        options:
            CallOptions(metadata: {"Authorization": "Bearer $accessToken"}));
    return result;
   }catch(e){
     print(["wdewef:",e.toString()]);
     goLogin(context);
   }
  }

  Future<MsgReply> networkDelete(BuildContext context,ID32 value) async {
    final client = AccWatchAPIClient(channel);
    final prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString("AccessToken");
    
    try{
     MsgReply result = await client.networkDelete(value,
        options:
            CallOptions(metadata: {"Authorization": "Bearer $accessToken"}));
    return result;
   }catch(e){
     print(["wdewef:",e.toString()]);
     goLogin(context);
   }
  }

  Future<NodeGroupList> getNodeGroupList(BuildContext context,) async {
    final client = AccWatchAPIClient(channel);
    final prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString("AccessToken");
    
    try{
     NodeGroupList result = await client.nodeGroupListGet(Empty(),
        options:
            CallOptions(metadata: {"Authorization": "Bearer $accessToken"}));
    return result;
   }catch(e){
     print(["wdewef:",e.toString()]);
     goLogin(context);
   }
  }

  Future<MsgReply> nodeGroupSet(BuildContext context,NodeGroup value) async {
    final client = AccWatchAPIClient(channel);
    final prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString("AccessToken");
    
    try{
     MsgReply result = await client.nodeGroupSet(value,
        options:
            CallOptions(metadata: {"Authorization": "Bearer $accessToken"}));
    return result;
   }catch(e){
     print(["wdewef:",e.toString()]);
     goLogin(context);
   }
  }

  Future<MsgReply> nodeGroupDelete(BuildContext context,ID32 value) async {
    final client = AccWatchAPIClient(channel);
    final prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString("AccessToken");
    
    try{
     MsgReply result = await client.nodeGroupDelete(value,
        options:
            CallOptions(metadata: {"Authorization": "Bearer $accessToken"}));
    return result;
   }catch(e){
     print(["wdewef:",e.toString()]);
     goLogin(context);
   }
  }

  Future<NotificationPolicyList> getNotificationPolicyList(BuildContext context,) async {
    final client = AccWatchAPIClient(channel);
    final prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString("AccessToken");
    
    try{
     NotificationPolicyList result =
        await client.notificationPolicyListGet(Empty(),
        options:
            CallOptions(metadata: {"Authorization": "Bearer $accessToken"}));
    return result;
   }catch(e){
     print(["wdewef:",e.toString()]);
     goLogin(context);
   }
  }

  Future<MsgReply> notificationPolicySet(BuildContext context,NotificationPolicy value) async {
    final client = AccWatchAPIClient(channel);
    final prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString("AccessToken");
    
    try{
     MsgReply result = await client.notificationPolicySet(value,
        options:
            CallOptions(metadata: {"Authorization": "Bearer $accessToken"}));
    return result;
   }catch(e){
     print(["wdewef:",e.toString()]);
     goLogin(context);
   }
  }

  Future<MsgReply> notificationPolicyDelete(BuildContext context,ID32 value) async {
    final client = AccWatchAPIClient(channel);
    final prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString("AccessToken");
    
    try{
     MsgReply result = await client.notificationPolicyDelete(value,
        options:
            CallOptions(metadata: {"Authorization": "Bearer $accessToken"}));
    return result;
   }catch(e){
     print(["wdewef:",e.toString()]);
     goLogin(context);
   }
  }

  Future<DomainCertificateList> getDomainCertificateList(BuildContext context) async {
    final client = AccWatchAPIClient(channel);
    final prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString("AccessToken");
    
    try{
     DomainCertificateList result =
        await client.domainCertificateListGet(Empty(),
        options:
            CallOptions(metadata: {"Authorization": "Bearer $accessToken"}));
    return result;
   }catch(e){
     print(["wdewef:",e.toString()]);
     goLogin(context);
   }
  }

  Future<Settings> getSetting(BuildContext context) async {
    final client = AccWatchAPIClient(channel);
    final prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString("AccessToken");
   try{
      Settings result = await client.settingsGet(Empty(),
        options:
            CallOptions(metadata: {"Authorization": "Bearer $accessToken"}));
    return result;
   }catch(e){
     print(["wdewef:",e.toString()]);
     goLogin(context);
   }
  }

  Future<MsgReply> settingSet(BuildContext context,Settings value) async {
    final client = AccWatchAPIClient(channel);
    final prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString("AccessToken");
   
    try{
      MsgReply result = await client.settingsSet(value,
        options:
            CallOptions(metadata: {"Authorization": "Bearer $accessToken"}));
    return result;
   }catch(e){
     print(["wdewef:",e.toString()]);
     goLogin(context);
   }
  }

  Future<Stream<NodeStatus>> nodeStatusStream(BuildContext context)async {
    final client = AccWatchAPIClient(channel);
    final prefs = await SharedPreferences.getInstance();
    String accessToken = prefs.getString("AccessToken");
   
    try{
      Stream<NodeStatus> result = client.nodeStatusStream(StreamRequest(milliseconds: 500),
        options:
            CallOptions(metadata: {"Authorization": "Bearer $accessToken"}));
    return result;
   }catch(e){
     print(["wdewef:",e.toString()]);
     goLogin(context);
   }
  }
  goLogin(BuildContext context){
    Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context)=>LoginPage()), (route) => false);
  }
}
