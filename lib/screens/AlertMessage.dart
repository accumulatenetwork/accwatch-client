import 'package:flutter/material.dart';

class AlertMessage extends StatelessWidget {
  final String title;
  final String content;
  final List<Widget> actions;

  AlertMessage({
    this.title,
    this.content,
    this.actions = const [],
  });

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(
        this.title,
        style: TextStyle(color: Colors.black, fontSize: 25),
      ),
      actions: this.actions,
      content: Text(
        this.content,
        style: TextStyle(color: Colors.black, fontSize: 18),
      ),
    );
  }
}