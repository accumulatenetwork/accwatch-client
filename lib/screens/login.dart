import 'package:flutter/material.dart';
import 'package:grpc_web_ks/login_service.dart';
import 'package:grpc_web_ks/screens/main_page.dart';
class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {

  TextEditingController _ctrlUser = TextEditingController(text: "admin");
  TextEditingController _ctrlPass = TextEditingController(text: "admin");
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text("Login Page"),
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 60.0),
              child: Center(
                child: Container(
                    width: 200,
                    height: 150,
                    child: Image.asset("assets/images/accumulate_tt.png")),
              ),
            ),
            Container(
              //padding: const EdgeInsets.only(left:15.0,right: 15.0,top:0,bottom: 0),
              width: 400,
              padding: EdgeInsets.symmetric(horizontal: 15),
              child: TextFormField(
                controller: _ctrlUser,
                validator: (value){
                  if(value.length==0){
                    return "Please input Username";
                  }else return null;
                },
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Username',
                    hintText: 'Enter username'),
              ),
            ),
            Container(
              width: 400,
              padding: const EdgeInsets.only(
                  left: 15.0, right: 15.0, top: 15, bottom: 0),
              //padding: EdgeInsets.symmetric(horizontal: 15),
              child: TextFormField(
                controller: _ctrlPass,
                obscureText: true,
                // ignore: missing_return
                validator: (value){
                  if(value.length==0){
                    return "Please input Password";
                  }else return null;
                },
                decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Password',
                    hintText: 'Enter secure password'),
              ),
            ),
            SizedBox(height: 20),
            Container(
              height: 50,
              width: 250,
              decoration: BoxDecoration(
                  color: Colors.blue, borderRadius: BorderRadius.circular(20)),
              child: FlatButton(
                onPressed: () async{
                  if(_formKey.currentState.validate()){
                   bool result = await LoginService().login(username: _ctrlUser.text,password: _ctrlPass.text);
                   if(result)
                   Navigator.push(
                      context, MaterialPageRoute(builder: (_) => MainPage(title:"No test")));
                    else
                      showAlert();
                  }
                },
                child: Text(
                  'Login',
                  style: TextStyle(color: Colors.white, fontSize: 25),
                ),
              ),
            ),
            SizedBox(
              height: 130,
            ),
            Text('New User? Create Account')
          ],
        )),
      ),
    );
  }
  showAlert(){
    return showDialog(
      context: context,
      builder: (BuildContext context){
        return Dialog(
          shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20.0)),
          child: Container(
            padding: EdgeInsets.fromLTRB(20, 20, 20, 20),
            height: 150,width: 250,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  child: Text("Login Failed!",style: TextStyle(fontSize: 30)),
                ),
                SizedBox(height: 20),
                ElevatedButton(onPressed: (){
                  Navigator.pop(context);
                }, child: Text("OK"))
              ],
            ),
          ),
        );
      }
    );
  }
}