import 'package:flutter/material.dart';
import 'package:grpc_web_ks/notification_data.dart';
import 'package:grpc_web_ks/provider/menu_provider.dart';
import 'package:grpc_web_ks/tabs/tab_node_groups.dart';
import 'package:grpc_web_ks/tabs/tab_nodes.dart';
import 'package:grpc_web_ks/tabs/tab_setting.dart';
import 'package:grpc_web_ks/tabs/tab_ssl_cert.dart';
import 'package:grpc_web_ks/tabs/tab_users.dart';
import 'package:grpc_web_ks/widget/navdrawer.dart';
import 'package:provider/provider.dart';

class MainPage extends StatefulWidget {
  MainPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MainPageState createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> {
  String _message = "nothing received yet";
  
  MenuProvider _menuProvider;
  
  int selectedIndex=0;

  double sysWidth=0.0;



  @override
  void initState() {
    super.initState();

    selectedIndex=0;
  }

  List<Widget> lstTab = [
    TabNodes(),
    TabNodeGroups(),
    TabSSLCert(),
    TabUsers(),
    TabSetting()
  ];

  @override
  Widget build(BuildContext context) {
    _menuProvider = Provider.of<MenuProvider>(context, listen: true);
    
    selectedIndex = _menuProvider.selectedIndex;
    sysWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      drawer: NavDrawer(selectedIndex: selectedIndex,onChange: (value){setState(() {
        _menuProvider.selectedIndex=value;
      });},),
      appBar: AppBar(
        title: Container(
            alignment: Alignment.centerLeft,
            height: 80,
            child: Image.asset("assets/images/accumulate_tt.png")),
      ),
      body: Container(
        child: Row(
          children: [
            sysWidth<600?SizedBox():Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin: Alignment.topRight,
                  end: Alignment.bottomLeft,
                  colors: [
                    Colors.blue,
                    Colors.white,
                  ],
                )
              ),
              width: 200,
                child: ListView(children: [
                ...tabTitle.asMap().map((key,value)=>MapEntry(key, Container(
                  color: selectedIndex==key?Colors.blue[800]:Colors.transparent,
                  child: ListTile(
                leading: Icon(Icons.input),
                title: Text(value.toString(),style: TextStyle(color: selectedIndex==key?Colors.white:Colors.black),),
                tileColor: selectedIndex==key?Colors.blue[800]:Colors.white,
                onTap: () {
                  setState(() {
                    _menuProvider.selectedIndex = key;
                  });
                },
              )),
                )).values.toList(),
            ]),
            ),
            Container(
              width: sysWidth>600?sysWidth-200:sysWidth,
              child: Stack(
                children: [
                  lstTab[selectedIndex],
                  // Container(
                  //   child: CircularProgressIndicator(),
                  // )
                 
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
