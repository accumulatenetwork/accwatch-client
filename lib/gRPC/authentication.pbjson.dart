///
//  Generated code. Do not modify.
//  source: authentication.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use authenticationRequestDescriptor instead')
const AuthenticationRequest$json = const {
  '1': 'AuthenticationRequest',
  '2': const [
    const {'1': 'Username', '3': 1, '4': 1, '5': 9, '10': 'Username'},
    const {'1': 'Password', '3': 2, '4': 1, '5': 9, '10': 'Password'},
  ],
};

/// Descriptor for `AuthenticationRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List authenticationRequestDescriptor = $convert.base64Decode('ChVBdXRoZW50aWNhdGlvblJlcXVlc3QSGgoIVXNlcm5hbWUYASABKAlSCFVzZXJuYW1lEhoKCFBhc3N3b3JkGAIgASgJUghQYXNzd29yZA==');
@$core.Deprecated('Use authenticationReplyDescriptor instead')
const AuthenticationReply$json = const {
  '1': 'AuthenticationReply',
  '2': const [
    const {'1': 'AccessToken', '3': 1, '4': 1, '5': 9, '10': 'AccessToken'},
    const {'1': 'ExpiresIn', '3': 2, '4': 1, '5': 5, '10': 'ExpiresIn'},
  ],
};

/// Descriptor for `AuthenticationReply`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List authenticationReplyDescriptor = $convert.base64Decode('ChNBdXRoZW50aWNhdGlvblJlcGx5EiAKC0FjY2Vzc1Rva2VuGAEgASgJUgtBY2Nlc3NUb2tlbhIcCglFeHBpcmVzSW4YAiABKAVSCUV4cGlyZXNJbg==');
