///
//  Generated code. Do not modify.
//  source: AccWatch.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:fixnum/fixnum.dart' as $fixnum;
import 'package:protobuf/protobuf.dart' as $pb;

import 'AccWatch.pbenum.dart';

export 'AccWatch.pbenum.dart';

class MsgReply extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'MsgReply', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'AccWatch'), createEmptyInstance: create)
    ..e<MsgReply_Status>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'status', $pb.PbFieldType.OE, defaultOrMaker: MsgReply_Status.fail, valueOf: MsgReply_Status.valueOf, enumValues: MsgReply_Status.values)
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'message')
    ..a<$core.int>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'newID32', $pb.PbFieldType.OU3, protoName: 'newID32')
    ..hasRequiredFields = false
  ;

  MsgReply._() : super();
  factory MsgReply({
    MsgReply_Status? status,
    $core.String? message,
    $core.int? newID32,
  }) {
    final _result = create();
    if (status != null) {
      _result.status = status;
    }
    if (message != null) {
      _result.message = message;
    }
    if (newID32 != null) {
      _result.newID32 = newID32;
    }
    return _result;
  }
  factory MsgReply.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory MsgReply.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  MsgReply clone() => MsgReply()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  MsgReply copyWith(void Function(MsgReply) updates) => super.copyWith((message) => updates(message as MsgReply)) as MsgReply; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static MsgReply create() => MsgReply._();
  MsgReply createEmptyInstance() => create();
  static $pb.PbList<MsgReply> createRepeated() => $pb.PbList<MsgReply>();
  @$core.pragma('dart2js:noInline')
  static MsgReply getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<MsgReply>(create);
  static MsgReply? _defaultInstance;

  @$pb.TagNumber(1)
  MsgReply_Status get status => $_getN(0);
  @$pb.TagNumber(1)
  set status(MsgReply_Status v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasStatus() => $_has(0);
  @$pb.TagNumber(1)
  void clearStatus() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get message => $_getSZ(1);
  @$pb.TagNumber(2)
  set message($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasMessage() => $_has(1);
  @$pb.TagNumber(2)
  void clearMessage() => clearField(2);

  @$pb.TagNumber(3)
  $core.int get newID32 => $_getIZ(2);
  @$pb.TagNumber(3)
  set newID32($core.int v) { $_setUnsignedInt32(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasNewID32() => $_has(2);
  @$pb.TagNumber(3)
  void clearNewID32() => clearField(3);
}

class ID32 extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'ID32', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'AccWatch'), createEmptyInstance: create)
    ..a<$core.int>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'ID', $pb.PbFieldType.OU3, protoName: 'ID')
    ..hasRequiredFields = false
  ;

  ID32._() : super();
  factory ID32({
    $core.int? iD,
  }) {
    final _result = create();
    if (iD != null) {
      _result.iD = iD;
    }
    return _result;
  }
  factory ID32.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ID32.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  ID32 clone() => ID32()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  ID32 copyWith(void Function(ID32) updates) => super.copyWith((message) => updates(message as ID32)) as ID32; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ID32 create() => ID32._();
  ID32 createEmptyInstance() => create();
  static $pb.PbList<ID32> createRepeated() => $pb.PbList<ID32>();
  @$core.pragma('dart2js:noInline')
  static ID32 getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ID32>(create);
  static ID32? _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get iD => $_getIZ(0);
  @$pb.TagNumber(1)
  set iD($core.int v) { $_setUnsignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasID() => $_has(0);
  @$pb.TagNumber(1)
  void clearID() => clearField(1);
}

class StreamRequest extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'StreamRequest', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'AccWatch'), createEmptyInstance: create)
    ..a<$core.int>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'Milliseconds', $pb.PbFieldType.OU3, protoName: 'Milliseconds')
    ..hasRequiredFields = false
  ;

  StreamRequest._() : super();
  factory StreamRequest({
    $core.int? milliseconds,
  }) {
    final _result = create();
    if (milliseconds != null) {
      _result.milliseconds = milliseconds;
    }
    return _result;
  }
  factory StreamRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory StreamRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  StreamRequest clone() => StreamRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  StreamRequest copyWith(void Function(StreamRequest) updates) => super.copyWith((message) => updates(message as StreamRequest)) as StreamRequest; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static StreamRequest create() => StreamRequest._();
  StreamRequest createEmptyInstance() => create();
  static $pb.PbList<StreamRequest> createRepeated() => $pb.PbList<StreamRequest>();
  @$core.pragma('dart2js:noInline')
  static StreamRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<StreamRequest>(create);
  static StreamRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get milliseconds => $_getIZ(0);
  @$pb.TagNumber(1)
  set milliseconds($core.int v) { $_setUnsignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasMilliseconds() => $_has(0);
  @$pb.TagNumber(1)
  void clearMilliseconds() => clearField(1);
}

class Authentication extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Authentication', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'AccWatch'), createEmptyInstance: create)
    ..a<$core.int>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'UserID', $pb.PbFieldType.OU3, protoName: 'UserID')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'Password', protoName: 'Password')
    ..e<Authentication_twoFAtype>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'TwoFAtype', $pb.PbFieldType.OE, protoName: 'TwoFAtype', defaultOrMaker: Authentication_twoFAtype.Yubikey, valueOf: Authentication_twoFAtype.valueOf, enumValues: Authentication_twoFAtype.values)
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'TwoFAData', protoName: 'TwoFAData')
    ..hasRequiredFields = false
  ;

  Authentication._() : super();
  factory Authentication({
    $core.int? userID,
    $core.String? password,
    Authentication_twoFAtype? twoFAtype,
    $core.String? twoFAData,
  }) {
    final _result = create();
    if (userID != null) {
      _result.userID = userID;
    }
    if (password != null) {
      _result.password = password;
    }
    if (twoFAtype != null) {
      _result.twoFAtype = twoFAtype;
    }
    if (twoFAData != null) {
      _result.twoFAData = twoFAData;
    }
    return _result;
  }
  factory Authentication.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Authentication.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Authentication clone() => Authentication()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Authentication copyWith(void Function(Authentication) updates) => super.copyWith((message) => updates(message as Authentication)) as Authentication; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Authentication create() => Authentication._();
  Authentication createEmptyInstance() => create();
  static $pb.PbList<Authentication> createRepeated() => $pb.PbList<Authentication>();
  @$core.pragma('dart2js:noInline')
  static Authentication getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Authentication>(create);
  static Authentication? _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get userID => $_getIZ(0);
  @$pb.TagNumber(1)
  set userID($core.int v) { $_setUnsignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasUserID() => $_has(0);
  @$pb.TagNumber(1)
  void clearUserID() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get password => $_getSZ(1);
  @$pb.TagNumber(2)
  set password($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasPassword() => $_has(1);
  @$pb.TagNumber(2)
  void clearPassword() => clearField(2);

  @$pb.TagNumber(3)
  Authentication_twoFAtype get twoFAtype => $_getN(2);
  @$pb.TagNumber(3)
  set twoFAtype(Authentication_twoFAtype v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasTwoFAtype() => $_has(2);
  @$pb.TagNumber(3)
  void clearTwoFAtype() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get twoFAData => $_getSZ(3);
  @$pb.TagNumber(4)
  set twoFAData($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasTwoFAData() => $_has(3);
  @$pb.TagNumber(4)
  void clearTwoFAData() => clearField(4);
}

class User extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'User', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'AccWatch'), createEmptyInstance: create)
    ..a<$core.int>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'UserID', $pb.PbFieldType.OU3, protoName: 'UserID')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'Name', protoName: 'Name')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'Email', protoName: 'Email')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'Tel', protoName: 'Tel')
    ..aOS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'Discord', protoName: 'Discord')
    ..a<$core.int>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'AccessRightsBits', $pb.PbFieldType.OU3, protoName: 'AccessRightsBits')
    ..hasRequiredFields = false
  ;

  User._() : super();
  factory User({
    $core.int? userID,
    $core.String? name,
    $core.String? email,
    $core.String? tel,
    $core.String? discord,
    $core.int? accessRightsBits,
  }) {
    final _result = create();
    if (userID != null) {
      _result.userID = userID;
    }
    if (name != null) {
      _result.name = name;
    }
    if (email != null) {
      _result.email = email;
    }
    if (tel != null) {
      _result.tel = tel;
    }
    if (discord != null) {
      _result.discord = discord;
    }
    if (accessRightsBits != null) {
      _result.accessRightsBits = accessRightsBits;
    }
    return _result;
  }
  factory User.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory User.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  User clone() => User()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  User copyWith(void Function(User) updates) => super.copyWith((message) => updates(message as User)) as User; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static User create() => User._();
  User createEmptyInstance() => create();
  static $pb.PbList<User> createRepeated() => $pb.PbList<User>();
  @$core.pragma('dart2js:noInline')
  static User getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<User>(create);
  static User? _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get userID => $_getIZ(0);
  @$pb.TagNumber(1)
  set userID($core.int v) { $_setUnsignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasUserID() => $_has(0);
  @$pb.TagNumber(1)
  void clearUserID() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get name => $_getSZ(1);
  @$pb.TagNumber(2)
  set name($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasName() => $_has(1);
  @$pb.TagNumber(2)
  void clearName() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get email => $_getSZ(2);
  @$pb.TagNumber(3)
  set email($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasEmail() => $_has(2);
  @$pb.TagNumber(3)
  void clearEmail() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get tel => $_getSZ(3);
  @$pb.TagNumber(4)
  set tel($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasTel() => $_has(3);
  @$pb.TagNumber(4)
  void clearTel() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get discord => $_getSZ(4);
  @$pb.TagNumber(5)
  set discord($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasDiscord() => $_has(4);
  @$pb.TagNumber(5)
  void clearDiscord() => clearField(5);

  @$pb.TagNumber(6)
  $core.int get accessRightsBits => $_getIZ(5);
  @$pb.TagNumber(6)
  set accessRightsBits($core.int v) { $_setUnsignedInt32(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasAccessRightsBits() => $_has(5);
  @$pb.TagNumber(6)
  void clearAccessRightsBits() => clearField(6);
}

class UserList extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'UserList', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'AccWatch'), createEmptyInstance: create)
    ..pc<User>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'users', $pb.PbFieldType.PM, subBuilder: User.create)
    ..hasRequiredFields = false
  ;

  UserList._() : super();
  factory UserList({
    $core.Iterable<User>? users,
  }) {
    final _result = create();
    if (users != null) {
      _result.users.addAll(users);
    }
    return _result;
  }
  factory UserList.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory UserList.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  UserList clone() => UserList()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  UserList copyWith(void Function(UserList) updates) => super.copyWith((message) => updates(message as UserList)) as UserList; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static UserList create() => UserList._();
  UserList createEmptyInstance() => create();
  static $pb.PbList<UserList> createRepeated() => $pb.PbList<UserList>();
  @$core.pragma('dart2js:noInline')
  static UserList getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<UserList>(create);
  static UserList? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<User> get users => $_getList(0);
}

class NotificationPolicy extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'NotificationPolicy', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'AccWatch'), createEmptyInstance: create)
    ..a<$core.int>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'NotifictionID', $pb.PbFieldType.OU3, protoName: 'NotifictionID')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'Name', protoName: 'Name')
    ..a<$core.int>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'Discord', $pb.PbFieldType.O3, protoName: 'Discord')
    ..a<$core.int>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'Call', $pb.PbFieldType.O3, protoName: 'Call')
    ..hasRequiredFields = false
  ;

  NotificationPolicy._() : super();
  factory NotificationPolicy({
    $core.int? notifictionID,
    $core.String? name,
    $core.int? discord,
    $core.int? call,
  }) {
    final _result = create();
    if (notifictionID != null) {
      _result.notifictionID = notifictionID;
    }
    if (name != null) {
      _result.name = name;
    }
    if (discord != null) {
      _result.discord = discord;
    }
    if (call != null) {
      _result.call = call;
    }
    return _result;
  }
  factory NotificationPolicy.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory NotificationPolicy.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  NotificationPolicy clone() => NotificationPolicy()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  NotificationPolicy copyWith(void Function(NotificationPolicy) updates) => super.copyWith((message) => updates(message as NotificationPolicy)) as NotificationPolicy; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static NotificationPolicy create() => NotificationPolicy._();
  NotificationPolicy createEmptyInstance() => create();
  static $pb.PbList<NotificationPolicy> createRepeated() => $pb.PbList<NotificationPolicy>();
  @$core.pragma('dart2js:noInline')
  static NotificationPolicy getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<NotificationPolicy>(create);
  static NotificationPolicy? _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get notifictionID => $_getIZ(0);
  @$pb.TagNumber(1)
  set notifictionID($core.int v) { $_setUnsignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasNotifictionID() => $_has(0);
  @$pb.TagNumber(1)
  void clearNotifictionID() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get name => $_getSZ(1);
  @$pb.TagNumber(2)
  set name($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasName() => $_has(1);
  @$pb.TagNumber(2)
  void clearName() => clearField(2);

  @$pb.TagNumber(3)
  $core.int get discord => $_getIZ(2);
  @$pb.TagNumber(3)
  set discord($core.int v) { $_setSignedInt32(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasDiscord() => $_has(2);
  @$pb.TagNumber(3)
  void clearDiscord() => clearField(3);

  @$pb.TagNumber(4)
  $core.int get call => $_getIZ(3);
  @$pb.TagNumber(4)
  set call($core.int v) { $_setSignedInt32(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasCall() => $_has(3);
  @$pb.TagNumber(4)
  void clearCall() => clearField(4);
}

class NotificationPolicyList extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'NotificationPolicyList', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'AccWatch'), createEmptyInstance: create)
    ..pc<NotificationPolicy>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'NotificationPolicyList', $pb.PbFieldType.PM, protoName: 'NotificationPolicyList', subBuilder: NotificationPolicy.create)
    ..hasRequiredFields = false
  ;

  NotificationPolicyList._() : super();
  factory NotificationPolicyList({
    $core.Iterable<NotificationPolicy>? notificationPolicyList,
  }) {
    final _result = create();
    if (notificationPolicyList != null) {
      _result.notificationPolicyList.addAll(notificationPolicyList);
    }
    return _result;
  }
  factory NotificationPolicyList.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory NotificationPolicyList.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  NotificationPolicyList clone() => NotificationPolicyList()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  NotificationPolicyList copyWith(void Function(NotificationPolicyList) updates) => super.copyWith((message) => updates(message as NotificationPolicyList)) as NotificationPolicyList; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static NotificationPolicyList create() => NotificationPolicyList._();
  NotificationPolicyList createEmptyInstance() => create();
  static $pb.PbList<NotificationPolicyList> createRepeated() => $pb.PbList<NotificationPolicyList>();
  @$core.pragma('dart2js:noInline')
  static NotificationPolicyList getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<NotificationPolicyList>(create);
  static NotificationPolicyList? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<NotificationPolicy> get notificationPolicyList => $_getList(0);
}

class Network extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Network', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'AccWatch'), createEmptyInstance: create)
    ..a<$core.int>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'NetworkID', $pb.PbFieldType.OU3, protoName: 'NetworkID')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'Name', protoName: 'Name')
    ..a<$core.int>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'BlockTime', $pb.PbFieldType.OU3, protoName: 'BlockTime')
    ..a<$core.int>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'StalledAfter', $pb.PbFieldType.OU3, protoName: 'StalledAfter')
    ..a<$core.int>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'NotifictionID', $pb.PbFieldType.OU3, protoName: 'NotifictionID')
    ..hasRequiredFields = false
  ;

  Network._() : super();
  factory Network({
    $core.int? networkID,
    $core.String? name,
    $core.int? blockTime,
    $core.int? stalledAfter,
    $core.int? notifictionID,
  }) {
    final _result = create();
    if (networkID != null) {
      _result.networkID = networkID;
    }
    if (name != null) {
      _result.name = name;
    }
    if (blockTime != null) {
      _result.blockTime = blockTime;
    }
    if (stalledAfter != null) {
      _result.stalledAfter = stalledAfter;
    }
    if (notifictionID != null) {
      _result.notifictionID = notifictionID;
    }
    return _result;
  }
  factory Network.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Network.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Network clone() => Network()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Network copyWith(void Function(Network) updates) => super.copyWith((message) => updates(message as Network)) as Network; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Network create() => Network._();
  Network createEmptyInstance() => create();
  static $pb.PbList<Network> createRepeated() => $pb.PbList<Network>();
  @$core.pragma('dart2js:noInline')
  static Network getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Network>(create);
  static Network? _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get networkID => $_getIZ(0);
  @$pb.TagNumber(1)
  set networkID($core.int v) { $_setUnsignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasNetworkID() => $_has(0);
  @$pb.TagNumber(1)
  void clearNetworkID() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get name => $_getSZ(1);
  @$pb.TagNumber(2)
  set name($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasName() => $_has(1);
  @$pb.TagNumber(2)
  void clearName() => clearField(2);

  @$pb.TagNumber(3)
  $core.int get blockTime => $_getIZ(2);
  @$pb.TagNumber(3)
  set blockTime($core.int v) { $_setUnsignedInt32(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasBlockTime() => $_has(2);
  @$pb.TagNumber(3)
  void clearBlockTime() => clearField(3);

  @$pb.TagNumber(4)
  $core.int get stalledAfter => $_getIZ(3);
  @$pb.TagNumber(4)
  set stalledAfter($core.int v) { $_setUnsignedInt32(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasStalledAfter() => $_has(3);
  @$pb.TagNumber(4)
  void clearStalledAfter() => clearField(4);

  @$pb.TagNumber(5)
  $core.int get notifictionID => $_getIZ(4);
  @$pb.TagNumber(5)
  set notifictionID($core.int v) { $_setUnsignedInt32(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasNotifictionID() => $_has(4);
  @$pb.TagNumber(5)
  void clearNotifictionID() => clearField(5);
}

class NetworkList extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'NetworkList', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'AccWatch'), createEmptyInstance: create)
    ..pc<Network>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'network', $pb.PbFieldType.PM, subBuilder: Network.create)
    ..hasRequiredFields = false
  ;

  NetworkList._() : super();
  factory NetworkList({
    $core.Iterable<Network>? network,
  }) {
    final _result = create();
    if (network != null) {
      _result.network.addAll(network);
    }
    return _result;
  }
  factory NetworkList.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory NetworkList.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  NetworkList clone() => NetworkList()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  NetworkList copyWith(void Function(NetworkList) updates) => super.copyWith((message) => updates(message as NetworkList)) as NetworkList; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static NetworkList create() => NetworkList._();
  NetworkList createEmptyInstance() => create();
  static $pb.PbList<NetworkList> createRepeated() => $pb.PbList<NetworkList>();
  @$core.pragma('dart2js:noInline')
  static NetworkList getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<NetworkList>(create);
  static NetworkList? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<Network> get network => $_getList(0);
}

class NetworkStatus extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'NetworkStatus', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'AccWatch'), createEmptyInstance: create)
    ..a<$core.int>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'NetworkID', $pb.PbFieldType.OU3, protoName: 'NetworkID')
    ..a<$fixnum.Int64>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'Height', $pb.PbFieldType.OU6, protoName: 'Height', defaultOrMaker: $fixnum.Int64.ZERO)
    ..a<$core.double>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'AverageTime', $pb.PbFieldType.OF, protoName: 'AverageTime')
    ..hasRequiredFields = false
  ;

  NetworkStatus._() : super();
  factory NetworkStatus({
    $core.int? networkID,
    $fixnum.Int64? height,
    $core.double? averageTime,
  }) {
    final _result = create();
    if (networkID != null) {
      _result.networkID = networkID;
    }
    if (height != null) {
      _result.height = height;
    }
    if (averageTime != null) {
      _result.averageTime = averageTime;
    }
    return _result;
  }
  factory NetworkStatus.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory NetworkStatus.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  NetworkStatus clone() => NetworkStatus()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  NetworkStatus copyWith(void Function(NetworkStatus) updates) => super.copyWith((message) => updates(message as NetworkStatus)) as NetworkStatus; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static NetworkStatus create() => NetworkStatus._();
  NetworkStatus createEmptyInstance() => create();
  static $pb.PbList<NetworkStatus> createRepeated() => $pb.PbList<NetworkStatus>();
  @$core.pragma('dart2js:noInline')
  static NetworkStatus getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<NetworkStatus>(create);
  static NetworkStatus? _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get networkID => $_getIZ(0);
  @$pb.TagNumber(1)
  set networkID($core.int v) { $_setUnsignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasNetworkID() => $_has(0);
  @$pb.TagNumber(1)
  void clearNetworkID() => clearField(1);

  @$pb.TagNumber(2)
  $fixnum.Int64 get height => $_getI64(1);
  @$pb.TagNumber(2)
  set height($fixnum.Int64 v) { $_setInt64(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasHeight() => $_has(1);
  @$pb.TagNumber(2)
  void clearHeight() => clearField(2);

  @$pb.TagNumber(3)
  $core.double get averageTime => $_getN(2);
  @$pb.TagNumber(3)
  set averageTime($core.double v) { $_setFloat(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasAverageTime() => $_has(2);
  @$pb.TagNumber(3)
  void clearAverageTime() => clearField(3);
}

class NodeGroup extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'NodeGroup', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'AccWatch'), createEmptyInstance: create)
    ..a<$core.int>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'NodeGroupID', $pb.PbFieldType.OU3, protoName: 'NodeGroupID')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'Name', protoName: 'Name')
    ..a<$core.int>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'NetworkID', $pb.PbFieldType.OU3, protoName: 'NetworkID')
    ..a<$core.int>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'PingNotifictionID', $pb.PbFieldType.OU3, protoName: 'PingNotifictionID')
    ..a<$core.int>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'HeightNotifictionID', $pb.PbFieldType.OU3, protoName: 'HeightNotifictionID')
    ..a<$core.int>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'LatencyNotifictionID', $pb.PbFieldType.OU3, protoName: 'LatencyNotifictionID')
    ..hasRequiredFields = false
  ;

  NodeGroup._() : super();
  factory NodeGroup({
    $core.int? nodeGroupID,
    $core.String? name,
    $core.int? networkID,
    $core.int? pingNotifictionID,
    $core.int? heightNotifictionID,
    $core.int? latencyNotifictionID,
  }) {
    final _result = create();
    if (nodeGroupID != null) {
      _result.nodeGroupID = nodeGroupID;
    }
    if (name != null) {
      _result.name = name;
    }
    if (networkID != null) {
      _result.networkID = networkID;
    }
    if (pingNotifictionID != null) {
      _result.pingNotifictionID = pingNotifictionID;
    }
    if (heightNotifictionID != null) {
      _result.heightNotifictionID = heightNotifictionID;
    }
    if (latencyNotifictionID != null) {
      _result.latencyNotifictionID = latencyNotifictionID;
    }
    return _result;
  }
  factory NodeGroup.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory NodeGroup.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  NodeGroup clone() => NodeGroup()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  NodeGroup copyWith(void Function(NodeGroup) updates) => super.copyWith((message) => updates(message as NodeGroup)) as NodeGroup; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static NodeGroup create() => NodeGroup._();
  NodeGroup createEmptyInstance() => create();
  static $pb.PbList<NodeGroup> createRepeated() => $pb.PbList<NodeGroup>();
  @$core.pragma('dart2js:noInline')
  static NodeGroup getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<NodeGroup>(create);
  static NodeGroup? _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get nodeGroupID => $_getIZ(0);
  @$pb.TagNumber(1)
  set nodeGroupID($core.int v) { $_setUnsignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasNodeGroupID() => $_has(0);
  @$pb.TagNumber(1)
  void clearNodeGroupID() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get name => $_getSZ(1);
  @$pb.TagNumber(2)
  set name($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasName() => $_has(1);
  @$pb.TagNumber(2)
  void clearName() => clearField(2);

  @$pb.TagNumber(3)
  $core.int get networkID => $_getIZ(2);
  @$pb.TagNumber(3)
  set networkID($core.int v) { $_setUnsignedInt32(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasNetworkID() => $_has(2);
  @$pb.TagNumber(3)
  void clearNetworkID() => clearField(3);

  @$pb.TagNumber(4)
  $core.int get pingNotifictionID => $_getIZ(3);
  @$pb.TagNumber(4)
  set pingNotifictionID($core.int v) { $_setUnsignedInt32(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasPingNotifictionID() => $_has(3);
  @$pb.TagNumber(4)
  void clearPingNotifictionID() => clearField(4);

  @$pb.TagNumber(5)
  $core.int get heightNotifictionID => $_getIZ(4);
  @$pb.TagNumber(5)
  set heightNotifictionID($core.int v) { $_setUnsignedInt32(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasHeightNotifictionID() => $_has(4);
  @$pb.TagNumber(5)
  void clearHeightNotifictionID() => clearField(5);

  @$pb.TagNumber(6)
  $core.int get latencyNotifictionID => $_getIZ(5);
  @$pb.TagNumber(6)
  set latencyNotifictionID($core.int v) { $_setUnsignedInt32(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasLatencyNotifictionID() => $_has(5);
  @$pb.TagNumber(6)
  void clearLatencyNotifictionID() => clearField(6);
}

class NodeGroupList extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'NodeGroupList', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'AccWatch'), createEmptyInstance: create)
    ..pc<NodeGroup>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'nodeGroup', $pb.PbFieldType.PM, protoName: 'nodeGroup', subBuilder: NodeGroup.create)
    ..hasRequiredFields = false
  ;

  NodeGroupList._() : super();
  factory NodeGroupList({
    $core.Iterable<NodeGroup>? nodeGroup,
  }) {
    final _result = create();
    if (nodeGroup != null) {
      _result.nodeGroup.addAll(nodeGroup);
    }
    return _result;
  }
  factory NodeGroupList.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory NodeGroupList.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  NodeGroupList clone() => NodeGroupList()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  NodeGroupList copyWith(void Function(NodeGroupList) updates) => super.copyWith((message) => updates(message as NodeGroupList)) as NodeGroupList; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static NodeGroupList create() => NodeGroupList._();
  NodeGroupList createEmptyInstance() => create();
  static $pb.PbList<NodeGroupList> createRepeated() => $pb.PbList<NodeGroupList>();
  @$core.pragma('dart2js:noInline')
  static NodeGroupList getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<NodeGroupList>(create);
  static NodeGroupList? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<NodeGroup> get nodeGroup => $_getList(0);
}

class Node extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Node', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'AccWatch'), createEmptyInstance: create)
    ..a<$core.int>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'NodeID', $pb.PbFieldType.OU3, protoName: 'NodeID')
    ..a<$core.int>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'NodeGroupID', $pb.PbFieldType.OU3, protoName: 'NodeGroupID')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'Name', protoName: 'Name')
    ..aOS(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'Host', protoName: 'Host')
    ..aOB(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'Monitor', protoName: 'Monitor')
    ..hasRequiredFields = false
  ;

  Node._() : super();
  factory Node({
    $core.int? nodeID,
    $core.int? nodeGroupID,
    $core.String? name,
    $core.String? host,
    $core.bool? monitor,
  }) {
    final _result = create();
    if (nodeID != null) {
      _result.nodeID = nodeID;
    }
    if (nodeGroupID != null) {
      _result.nodeGroupID = nodeGroupID;
    }
    if (name != null) {
      _result.name = name;
    }
    if (host != null) {
      _result.host = host;
    }
    if (monitor != null) {
      _result.monitor = monitor;
    }
    return _result;
  }
  factory Node.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Node.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Node clone() => Node()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Node copyWith(void Function(Node) updates) => super.copyWith((message) => updates(message as Node)) as Node; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Node create() => Node._();
  Node createEmptyInstance() => create();
  static $pb.PbList<Node> createRepeated() => $pb.PbList<Node>();
  @$core.pragma('dart2js:noInline')
  static Node getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Node>(create);
  static Node? _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get nodeID => $_getIZ(0);
  @$pb.TagNumber(1)
  set nodeID($core.int v) { $_setUnsignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasNodeID() => $_has(0);
  @$pb.TagNumber(1)
  void clearNodeID() => clearField(1);

  @$pb.TagNumber(2)
  $core.int get nodeGroupID => $_getIZ(1);
  @$pb.TagNumber(2)
  set nodeGroupID($core.int v) { $_setUnsignedInt32(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasNodeGroupID() => $_has(1);
  @$pb.TagNumber(2)
  void clearNodeGroupID() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get name => $_getSZ(2);
  @$pb.TagNumber(3)
  set name($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasName() => $_has(2);
  @$pb.TagNumber(3)
  void clearName() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get host => $_getSZ(3);
  @$pb.TagNumber(4)
  set host($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasHost() => $_has(3);
  @$pb.TagNumber(4)
  void clearHost() => clearField(4);

  @$pb.TagNumber(5)
  $core.bool get monitor => $_getBF(4);
  @$pb.TagNumber(5)
  set monitor($core.bool v) { $_setBool(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasMonitor() => $_has(4);
  @$pb.TagNumber(5)
  void clearMonitor() => clearField(5);
}

class NodeStatus extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'NodeStatus', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'AccWatch'), createEmptyInstance: create)
    ..a<$core.int>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'NodeID', $pb.PbFieldType.OU3, protoName: 'NodeID')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'Version', protoName: 'Version')
    ..a<$fixnum.Int64>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'Height', $pb.PbFieldType.OU6, protoName: 'Height', defaultOrMaker: $fixnum.Int64.ZERO)
    ..a<$core.double>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'Ping', $pb.PbFieldType.OF, protoName: 'Ping')
    ..hasRequiredFields = false
  ;

  NodeStatus._() : super();
  factory NodeStatus({
    $core.int? nodeID,
    $core.String? version,
    $fixnum.Int64? height,
    $core.double? ping,
  }) {
    final _result = create();
    if (nodeID != null) {
      _result.nodeID = nodeID;
    }
    if (version != null) {
      _result.version = version;
    }
    if (height != null) {
      _result.height = height;
    }
    if (ping != null) {
      _result.ping = ping;
    }
    return _result;
  }
  factory NodeStatus.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory NodeStatus.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  NodeStatus clone() => NodeStatus()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  NodeStatus copyWith(void Function(NodeStatus) updates) => super.copyWith((message) => updates(message as NodeStatus)) as NodeStatus; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static NodeStatus create() => NodeStatus._();
  NodeStatus createEmptyInstance() => create();
  static $pb.PbList<NodeStatus> createRepeated() => $pb.PbList<NodeStatus>();
  @$core.pragma('dart2js:noInline')
  static NodeStatus getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<NodeStatus>(create);
  static NodeStatus? _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get nodeID => $_getIZ(0);
  @$pb.TagNumber(1)
  set nodeID($core.int v) { $_setUnsignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasNodeID() => $_has(0);
  @$pb.TagNumber(1)
  void clearNodeID() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get version => $_getSZ(1);
  @$pb.TagNumber(2)
  set version($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasVersion() => $_has(1);
  @$pb.TagNumber(2)
  void clearVersion() => clearField(2);

  @$pb.TagNumber(3)
  $fixnum.Int64 get height => $_getI64(2);
  @$pb.TagNumber(3)
  set height($fixnum.Int64 v) { $_setInt64(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasHeight() => $_has(2);
  @$pb.TagNumber(3)
  void clearHeight() => clearField(3);

  @$pb.TagNumber(4)
  $core.double get ping => $_getN(3);
  @$pb.TagNumber(4)
  set ping($core.double v) { $_setFloat(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasPing() => $_has(3);
  @$pb.TagNumber(4)
  void clearPing() => clearField(4);
}

class NodeList extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'NodeList', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'AccWatch'), createEmptyInstance: create)
    ..pc<Node>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'nodes', $pb.PbFieldType.PM, subBuilder: Node.create)
    ..hasRequiredFields = false
  ;

  NodeList._() : super();
  factory NodeList({
    $core.Iterable<Node>? nodes,
  }) {
    final _result = create();
    if (nodes != null) {
      _result.nodes.addAll(nodes);
    }
    return _result;
  }
  factory NodeList.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory NodeList.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  NodeList clone() => NodeList()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  NodeList copyWith(void Function(NodeList) updates) => super.copyWith((message) => updates(message as NodeList)) as NodeList; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static NodeList create() => NodeList._();
  NodeList createEmptyInstance() => create();
  static $pb.PbList<NodeList> createRepeated() => $pb.PbList<NodeList>();
  @$core.pragma('dart2js:noInline')
  static NodeList getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<NodeList>(create);
  static NodeList? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<Node> get nodes => $_getList(0);
}

class DomainCertificate extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'DomainCertificate', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'AccWatch'), createEmptyInstance: create)
    ..a<$core.int>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'DomainCertificateID', $pb.PbFieldType.OU3, protoName: 'DomainCertificateID')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'Domain', protoName: 'Domain')
    ..aOB(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'Monitor', protoName: 'Monitor')
    ..hasRequiredFields = false
  ;

  DomainCertificate._() : super();
  factory DomainCertificate({
    $core.int? domainCertificateID,
    $core.String? domain,
    $core.bool? monitor,
  }) {
    final _result = create();
    if (domainCertificateID != null) {
      _result.domainCertificateID = domainCertificateID;
    }
    if (domain != null) {
      _result.domain = domain;
    }
    if (monitor != null) {
      _result.monitor = monitor;
    }
    return _result;
  }
  factory DomainCertificate.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory DomainCertificate.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  DomainCertificate clone() => DomainCertificate()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  DomainCertificate copyWith(void Function(DomainCertificate) updates) => super.copyWith((message) => updates(message as DomainCertificate)) as DomainCertificate; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static DomainCertificate create() => DomainCertificate._();
  DomainCertificate createEmptyInstance() => create();
  static $pb.PbList<DomainCertificate> createRepeated() => $pb.PbList<DomainCertificate>();
  @$core.pragma('dart2js:noInline')
  static DomainCertificate getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<DomainCertificate>(create);
  static DomainCertificate? _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get domainCertificateID => $_getIZ(0);
  @$pb.TagNumber(1)
  set domainCertificateID($core.int v) { $_setUnsignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasDomainCertificateID() => $_has(0);
  @$pb.TagNumber(1)
  void clearDomainCertificateID() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get domain => $_getSZ(1);
  @$pb.TagNumber(2)
  set domain($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasDomain() => $_has(1);
  @$pb.TagNumber(2)
  void clearDomain() => clearField(2);

  @$pb.TagNumber(3)
  $core.bool get monitor => $_getBF(2);
  @$pb.TagNumber(3)
  set monitor($core.bool v) { $_setBool(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasMonitor() => $_has(2);
  @$pb.TagNumber(3)
  void clearMonitor() => clearField(3);
}

class DomainCertificateList extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'DomainCertificateList', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'AccWatch'), createEmptyInstance: create)
    ..pc<DomainCertificate>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'domainCertificate', $pb.PbFieldType.PM, protoName: 'domainCertificate', subBuilder: DomainCertificate.create)
    ..hasRequiredFields = false
  ;

  DomainCertificateList._() : super();
  factory DomainCertificateList({
    $core.Iterable<DomainCertificate>? domainCertificate,
  }) {
    final _result = create();
    if (domainCertificate != null) {
      _result.domainCertificate.addAll(domainCertificate);
    }
    return _result;
  }
  factory DomainCertificateList.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory DomainCertificateList.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  DomainCertificateList clone() => DomainCertificateList()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  DomainCertificateList copyWith(void Function(DomainCertificateList) updates) => super.copyWith((message) => updates(message as DomainCertificateList)) as DomainCertificateList; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static DomainCertificateList create() => DomainCertificateList._();
  DomainCertificateList createEmptyInstance() => create();
  static $pb.PbList<DomainCertificateList> createRepeated() => $pb.PbList<DomainCertificateList>();
  @$core.pragma('dart2js:noInline')
  static DomainCertificateList getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<DomainCertificateList>(create);
  static DomainCertificateList? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<DomainCertificate> get domainCertificate => $_getList(0);
}

class Settings extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'Settings', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'AccWatch'), createEmptyInstance: create)
    ..aOS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'BotName', protoName: 'BotName')
    ..aOS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'DiscordClientID', protoName: 'DiscordClientID')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'DiscordToken', protoName: 'DiscordToken')
    ..a<$fixnum.Int64>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'AccumulateOperatorAlertsCh', $pb.PbFieldType.OU6, protoName: 'AccumulateOperatorAlertsCh', defaultOrMaker: $fixnum.Int64.ZERO)
    ..aOS(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'DiscordAlertsChannel', protoName: 'DiscordAlertsChannel')
    ..aOS(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'SIPUsername', protoName: 'SIPUsername')
    ..aOS(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'SIPPassword', protoName: 'SIPPassword')
    ..aOS(8, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'SIPHost', protoName: 'SIPHost')
    ..aOS(9, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'SIPCallingNumber', protoName: 'SIPCallingNumber')
    ..aOS(10, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'TwimletURL', protoName: 'TwimletURL')
    ..a<$core.int>(11, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'AlarmOffWarningMinutes', $pb.PbFieldType.OU3, protoName: 'AlarmOffWarningMinutes')
    ..a<$core.int>(12, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'LatencyTriggerMultiplier', $pb.PbFieldType.OU3, protoName: 'LatencyTriggerMultiplier')
    ..aOS(13, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'BotCommandPrefix', protoName: 'BotCommandPrefix')
    ..aOS(14, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'EmailSMTPHost', protoName: 'EmailSMTPHost')
    ..a<$core.int>(15, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'EmailSMTPPort', $pb.PbFieldType.OU3, protoName: 'EmailSMTPPort')
    ..aOS(16, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'EmailUsername', protoName: 'EmailUsername')
    ..aOS(17, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'EmailPassword', protoName: 'EmailPassword')
    ..aOS(18, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'EmailFromAddress', protoName: 'EmailFromAddress')
    ..hasRequiredFields = false
  ;

  Settings._() : super();
  factory Settings({
    $core.String? botName,
    $core.String? discordClientID,
    $core.String? discordToken,
    $fixnum.Int64? accumulateOperatorAlertsCh,
    $core.String? discordAlertsChannel,
    $core.String? sIPUsername,
    $core.String? sIPPassword,
    $core.String? sIPHost,
    $core.String? sIPCallingNumber,
    $core.String? twimletURL,
    $core.int? alarmOffWarningMinutes,
    $core.int? latencyTriggerMultiplier,
    $core.String? botCommandPrefix,
    $core.String? emailSMTPHost,
    $core.int? emailSMTPPort,
    $core.String? emailUsername,
    $core.String? emailPassword,
    $core.String? emailFromAddress,
  }) {
    final _result = create();
    if (botName != null) {
      _result.botName = botName;
    }
    if (discordClientID != null) {
      _result.discordClientID = discordClientID;
    }
    if (discordToken != null) {
      _result.discordToken = discordToken;
    }
    if (accumulateOperatorAlertsCh != null) {
      _result.accumulateOperatorAlertsCh = accumulateOperatorAlertsCh;
    }
    if (discordAlertsChannel != null) {
      _result.discordAlertsChannel = discordAlertsChannel;
    }
    if (sIPUsername != null) {
      _result.sIPUsername = sIPUsername;
    }
    if (sIPPassword != null) {
      _result.sIPPassword = sIPPassword;
    }
    if (sIPHost != null) {
      _result.sIPHost = sIPHost;
    }
    if (sIPCallingNumber != null) {
      _result.sIPCallingNumber = sIPCallingNumber;
    }
    if (twimletURL != null) {
      _result.twimletURL = twimletURL;
    }
    if (alarmOffWarningMinutes != null) {
      _result.alarmOffWarningMinutes = alarmOffWarningMinutes;
    }
    if (latencyTriggerMultiplier != null) {
      _result.latencyTriggerMultiplier = latencyTriggerMultiplier;
    }
    if (botCommandPrefix != null) {
      _result.botCommandPrefix = botCommandPrefix;
    }
    if (emailSMTPHost != null) {
      _result.emailSMTPHost = emailSMTPHost;
    }
    if (emailSMTPPort != null) {
      _result.emailSMTPPort = emailSMTPPort;
    }
    if (emailUsername != null) {
      _result.emailUsername = emailUsername;
    }
    if (emailPassword != null) {
      _result.emailPassword = emailPassword;
    }
    if (emailFromAddress != null) {
      _result.emailFromAddress = emailFromAddress;
    }
    return _result;
  }
  factory Settings.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Settings.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Settings clone() => Settings()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Settings copyWith(void Function(Settings) updates) => super.copyWith((message) => updates(message as Settings)) as Settings; // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Settings create() => Settings._();
  Settings createEmptyInstance() => create();
  static $pb.PbList<Settings> createRepeated() => $pb.PbList<Settings>();
  @$core.pragma('dart2js:noInline')
  static Settings getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Settings>(create);
  static Settings? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get botName => $_getSZ(0);
  @$pb.TagNumber(1)
  set botName($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasBotName() => $_has(0);
  @$pb.TagNumber(1)
  void clearBotName() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get discordClientID => $_getSZ(1);
  @$pb.TagNumber(2)
  set discordClientID($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasDiscordClientID() => $_has(1);
  @$pb.TagNumber(2)
  void clearDiscordClientID() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get discordToken => $_getSZ(2);
  @$pb.TagNumber(3)
  set discordToken($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasDiscordToken() => $_has(2);
  @$pb.TagNumber(3)
  void clearDiscordToken() => clearField(3);

  @$pb.TagNumber(4)
  $fixnum.Int64 get accumulateOperatorAlertsCh => $_getI64(3);
  @$pb.TagNumber(4)
  set accumulateOperatorAlertsCh($fixnum.Int64 v) { $_setInt64(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasAccumulateOperatorAlertsCh() => $_has(3);
  @$pb.TagNumber(4)
  void clearAccumulateOperatorAlertsCh() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get discordAlertsChannel => $_getSZ(4);
  @$pb.TagNumber(5)
  set discordAlertsChannel($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasDiscordAlertsChannel() => $_has(4);
  @$pb.TagNumber(5)
  void clearDiscordAlertsChannel() => clearField(5);

  @$pb.TagNumber(6)
  $core.String get sIPUsername => $_getSZ(5);
  @$pb.TagNumber(6)
  set sIPUsername($core.String v) { $_setString(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasSIPUsername() => $_has(5);
  @$pb.TagNumber(6)
  void clearSIPUsername() => clearField(6);

  @$pb.TagNumber(7)
  $core.String get sIPPassword => $_getSZ(6);
  @$pb.TagNumber(7)
  set sIPPassword($core.String v) { $_setString(6, v); }
  @$pb.TagNumber(7)
  $core.bool hasSIPPassword() => $_has(6);
  @$pb.TagNumber(7)
  void clearSIPPassword() => clearField(7);

  @$pb.TagNumber(8)
  $core.String get sIPHost => $_getSZ(7);
  @$pb.TagNumber(8)
  set sIPHost($core.String v) { $_setString(7, v); }
  @$pb.TagNumber(8)
  $core.bool hasSIPHost() => $_has(7);
  @$pb.TagNumber(8)
  void clearSIPHost() => clearField(8);

  @$pb.TagNumber(9)
  $core.String get sIPCallingNumber => $_getSZ(8);
  @$pb.TagNumber(9)
  set sIPCallingNumber($core.String v) { $_setString(8, v); }
  @$pb.TagNumber(9)
  $core.bool hasSIPCallingNumber() => $_has(8);
  @$pb.TagNumber(9)
  void clearSIPCallingNumber() => clearField(9);

  @$pb.TagNumber(10)
  $core.String get twimletURL => $_getSZ(9);
  @$pb.TagNumber(10)
  set twimletURL($core.String v) { $_setString(9, v); }
  @$pb.TagNumber(10)
  $core.bool hasTwimletURL() => $_has(9);
  @$pb.TagNumber(10)
  void clearTwimletURL() => clearField(10);

  @$pb.TagNumber(11)
  $core.int get alarmOffWarningMinutes => $_getIZ(10);
  @$pb.TagNumber(11)
  set alarmOffWarningMinutes($core.int v) { $_setUnsignedInt32(10, v); }
  @$pb.TagNumber(11)
  $core.bool hasAlarmOffWarningMinutes() => $_has(10);
  @$pb.TagNumber(11)
  void clearAlarmOffWarningMinutes() => clearField(11);

  @$pb.TagNumber(12)
  $core.int get latencyTriggerMultiplier => $_getIZ(11);
  @$pb.TagNumber(12)
  set latencyTriggerMultiplier($core.int v) { $_setUnsignedInt32(11, v); }
  @$pb.TagNumber(12)
  $core.bool hasLatencyTriggerMultiplier() => $_has(11);
  @$pb.TagNumber(12)
  void clearLatencyTriggerMultiplier() => clearField(12);

  @$pb.TagNumber(13)
  $core.String get botCommandPrefix => $_getSZ(12);
  @$pb.TagNumber(13)
  set botCommandPrefix($core.String v) { $_setString(12, v); }
  @$pb.TagNumber(13)
  $core.bool hasBotCommandPrefix() => $_has(12);
  @$pb.TagNumber(13)
  void clearBotCommandPrefix() => clearField(13);

  @$pb.TagNumber(14)
  $core.String get emailSMTPHost => $_getSZ(13);
  @$pb.TagNumber(14)
  set emailSMTPHost($core.String v) { $_setString(13, v); }
  @$pb.TagNumber(14)
  $core.bool hasEmailSMTPHost() => $_has(13);
  @$pb.TagNumber(14)
  void clearEmailSMTPHost() => clearField(14);

  @$pb.TagNumber(15)
  $core.int get emailSMTPPort => $_getIZ(14);
  @$pb.TagNumber(15)
  set emailSMTPPort($core.int v) { $_setUnsignedInt32(14, v); }
  @$pb.TagNumber(15)
  $core.bool hasEmailSMTPPort() => $_has(14);
  @$pb.TagNumber(15)
  void clearEmailSMTPPort() => clearField(15);

  @$pb.TagNumber(16)
  $core.String get emailUsername => $_getSZ(15);
  @$pb.TagNumber(16)
  set emailUsername($core.String v) { $_setString(15, v); }
  @$pb.TagNumber(16)
  $core.bool hasEmailUsername() => $_has(15);
  @$pb.TagNumber(16)
  void clearEmailUsername() => clearField(16);

  @$pb.TagNumber(17)
  $core.String get emailPassword => $_getSZ(16);
  @$pb.TagNumber(17)
  set emailPassword($core.String v) { $_setString(16, v); }
  @$pb.TagNumber(17)
  $core.bool hasEmailPassword() => $_has(16);
  @$pb.TagNumber(17)
  void clearEmailPassword() => clearField(17);

  @$pb.TagNumber(18)
  $core.String get emailFromAddress => $_getSZ(17);
  @$pb.TagNumber(18)
  set emailFromAddress($core.String v) { $_setString(17, v); }
  @$pb.TagNumber(18)
  $core.bool hasEmailFromAddress() => $_has(17);
  @$pb.TagNumber(18)
  void clearEmailFromAddress() => clearField(18);
}

