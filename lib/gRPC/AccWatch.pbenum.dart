///
//  Generated code. Do not modify.
//  source: AccWatch.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

// ignore_for_file: UNDEFINED_SHOWN_NAME
import 'dart:core' as $core;
import 'package:protobuf/protobuf.dart' as $pb;

class MessageType extends $pb.ProtobufEnum {
  static const MessageType Add = MessageType._(0, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'Add');
  static const MessageType Update = MessageType._(1, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'Update');
  static const MessageType Remove = MessageType._(2, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'Remove');

  static const $core.List<MessageType> values = <MessageType> [
    Add,
    Update,
    Remove,
  ];

  static final $core.Map<$core.int, MessageType> _byValue = $pb.ProtobufEnum.initByValue(values);
  static MessageType? valueOf($core.int value) => _byValue[value];

  const MessageType._($core.int v, $core.String n) : super(v, n);
}

class MsgReply_Status extends $pb.ProtobufEnum {
  static const MsgReply_Status fail = MsgReply_Status._(0, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'fail');
  static const MsgReply_Status ok = MsgReply_Status._(1, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'ok');

  static const $core.List<MsgReply_Status> values = <MsgReply_Status> [
    fail,
    ok,
  ];

  static final $core.Map<$core.int, MsgReply_Status> _byValue = $pb.ProtobufEnum.initByValue(values);
  static MsgReply_Status? valueOf($core.int value) => _byValue[value];

  const MsgReply_Status._($core.int v, $core.String n) : super(v, n);
}

class Authentication_twoFAtype extends $pb.ProtobufEnum {
  static const Authentication_twoFAtype Yubikey = Authentication_twoFAtype._(0, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'Yubikey');

  static const $core.List<Authentication_twoFAtype> values = <Authentication_twoFAtype> [
    Yubikey,
  ];

  static final $core.Map<$core.int, Authentication_twoFAtype> _byValue = $pb.ProtobufEnum.initByValue(values);
  static Authentication_twoFAtype? valueOf($core.int value) => _byValue[value];

  const Authentication_twoFAtype._($core.int v, $core.String n) : super(v, n);
}

class User_AccessRights extends $pb.ProtobufEnum {
  static const User_AccessRights Read = User_AccessRights._(0, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'Read');
  static const User_AccessRights Write = User_AccessRights._(1, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'Write');
  static const User_AccessRights Users = User_AccessRights._(2, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'Users');
  static const User_AccessRights Settings = User_AccessRights._(4, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'Settings');

  static const $core.List<User_AccessRights> values = <User_AccessRights> [
    Read,
    Write,
    Users,
    Settings,
  ];

  static final $core.Map<$core.int, User_AccessRights> _byValue = $pb.ProtobufEnum.initByValue(values);
  static User_AccessRights? valueOf($core.int value) => _byValue[value];

  const User_AccessRights._($core.int v, $core.String n) : super(v, n);
}

