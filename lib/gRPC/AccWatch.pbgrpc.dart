///
//  Generated code. Do not modify.
//  source: AccWatch.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'google/protobuf/empty.pb.dart' as $0;
import 'AccWatch.pb.dart' as $1;
export 'AccWatch.pb.dart';

class AccWatchAPIClient extends $grpc.Client {
  static final _$notificationPolicyListGet =
      $grpc.ClientMethod<$0.Empty, $1.NotificationPolicyList>(
          '/AccWatch.AccWatchAPI/NotificationPolicyListGet',
          ($0.Empty value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $1.NotificationPolicyList.fromBuffer(value));
  static final _$notificationPolicySet =
      $grpc.ClientMethod<$1.NotificationPolicy, $1.MsgReply>(
          '/AccWatch.AccWatchAPI/NotificationPolicySet',
          ($1.NotificationPolicy value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $1.MsgReply.fromBuffer(value));
  static final _$notificationPolicyDelete =
      $grpc.ClientMethod<$1.ID32, $1.MsgReply>(
          '/AccWatch.AccWatchAPI/NotificationPolicyDelete',
          ($1.ID32 value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $1.MsgReply.fromBuffer(value));
  static final _$networkListGet = $grpc.ClientMethod<$0.Empty, $1.NetworkList>(
      '/AccWatch.AccWatchAPI/NetworkListGet',
      ($0.Empty value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $1.NetworkList.fromBuffer(value));
  static final _$networkSet = $grpc.ClientMethod<$1.Network, $1.MsgReply>(
      '/AccWatch.AccWatchAPI/NetworkSet',
      ($1.Network value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $1.MsgReply.fromBuffer(value));
  static final _$networkDelete = $grpc.ClientMethod<$1.ID32, $1.MsgReply>(
      '/AccWatch.AccWatchAPI/NetworkDelete',
      ($1.ID32 value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $1.MsgReply.fromBuffer(value));
  static final _$networkStatusStream =
      $grpc.ClientMethod<$1.StreamRequest, $1.NetworkStatus>(
          '/AccWatch.AccWatchAPI/NetworkStatusStream',
          ($1.StreamRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $1.NetworkStatus.fromBuffer(value));
  static final _$nodeGroupListGet =
      $grpc.ClientMethod<$0.Empty, $1.NodeGroupList>(
          '/AccWatch.AccWatchAPI/NodeGroupListGet',
          ($0.Empty value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $1.NodeGroupList.fromBuffer(value));
  static final _$nodeGroupSet = $grpc.ClientMethod<$1.NodeGroup, $1.MsgReply>(
      '/AccWatch.AccWatchAPI/NodeGroupSet',
      ($1.NodeGroup value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $1.MsgReply.fromBuffer(value));
  static final _$nodeGroupDelete = $grpc.ClientMethod<$1.ID32, $1.MsgReply>(
      '/AccWatch.AccWatchAPI/NodeGroupDelete',
      ($1.ID32 value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $1.MsgReply.fromBuffer(value));
  static final _$nodeListGet = $grpc.ClientMethod<$0.Empty, $1.NodeList>(
      '/AccWatch.AccWatchAPI/NodeListGet',
      ($0.Empty value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $1.NodeList.fromBuffer(value));
  static final _$nodeSet = $grpc.ClientMethod<$1.Node, $1.MsgReply>(
      '/AccWatch.AccWatchAPI/NodeSet',
      ($1.Node value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $1.MsgReply.fromBuffer(value));
  static final _$nodeDelete = $grpc.ClientMethod<$1.ID32, $1.MsgReply>(
      '/AccWatch.AccWatchAPI/NodeDelete',
      ($1.ID32 value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $1.MsgReply.fromBuffer(value));
  static final _$nodeStatusStream =
      $grpc.ClientMethod<$1.StreamRequest, $1.NodeStatus>(
          '/AccWatch.AccWatchAPI/NodeStatusStream',
          ($1.StreamRequest value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $1.NodeStatus.fromBuffer(value));
  static final _$domainCertificateListGet =
      $grpc.ClientMethod<$0.Empty, $1.DomainCertificateList>(
          '/AccWatch.AccWatchAPI/DomainCertificateListGet',
          ($0.Empty value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $1.DomainCertificateList.fromBuffer(value));
  static final _$domainCertificateSet =
      $grpc.ClientMethod<$1.DomainCertificate, $1.MsgReply>(
          '/AccWatch.AccWatchAPI/DomainCertificateSet',
          ($1.DomainCertificate value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $1.MsgReply.fromBuffer(value));
  static final _$domainCertificateDelete =
      $grpc.ClientMethod<$1.ID32, $1.MsgReply>(
          '/AccWatch.AccWatchAPI/DomainCertificateDelete',
          ($1.ID32 value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $1.MsgReply.fromBuffer(value));
  static final _$settingsGet = $grpc.ClientMethod<$0.Empty, $1.Settings>(
      '/AccWatch.AccWatchAPI/SettingsGet',
      ($0.Empty value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $1.Settings.fromBuffer(value));
  static final _$settingsSet = $grpc.ClientMethod<$1.Settings, $1.MsgReply>(
      '/AccWatch.AccWatchAPI/SettingsSet',
      ($1.Settings value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $1.MsgReply.fromBuffer(value));
  static final _$userListGet = $grpc.ClientMethod<$0.Empty, $1.UserList>(
      '/AccWatch.AccWatchAPI/UserListGet',
      ($0.Empty value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $1.UserList.fromBuffer(value));
  static final _$userSet = $grpc.ClientMethod<$1.User, $1.MsgReply>(
      '/AccWatch.AccWatchAPI/UserSet',
      ($1.User value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $1.MsgReply.fromBuffer(value));
  static final _$userDelete = $grpc.ClientMethod<$1.ID32, $1.MsgReply>(
      '/AccWatch.AccWatchAPI/UserDelete',
      ($1.ID32 value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $1.MsgReply.fromBuffer(value));

  AccWatchAPIClient($grpc.ClientChannel channel,
      {$grpc.CallOptions? options,
      $core.Iterable<$grpc.ClientInterceptor>? interceptors})
      : super(channel, options: options, interceptors: interceptors);

  $grpc.ResponseFuture<$1.NotificationPolicyList> notificationPolicyListGet(
      $0.Empty request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$notificationPolicyListGet, request,
        options: options);
  }

  $grpc.ResponseFuture<$1.MsgReply> notificationPolicySet(
      $1.NotificationPolicy request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$notificationPolicySet, request, options: options);
  }

  $grpc.ResponseFuture<$1.MsgReply> notificationPolicyDelete($1.ID32 request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$notificationPolicyDelete, request,
        options: options);
  }

  $grpc.ResponseFuture<$1.NetworkList> networkListGet($0.Empty request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$networkListGet, request, options: options);
  }

  $grpc.ResponseFuture<$1.MsgReply> networkSet($1.Network request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$networkSet, request, options: options);
  }

  $grpc.ResponseFuture<$1.MsgReply> networkDelete($1.ID32 request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$networkDelete, request, options: options);
  }

  $grpc.ResponseStream<$1.NetworkStatus> networkStatusStream(
      $1.StreamRequest request,
      {$grpc.CallOptions? options}) {
    return $createStreamingCall(
        _$networkStatusStream, $async.Stream.fromIterable([request]),
        options: options);
  }

  $grpc.ResponseFuture<$1.NodeGroupList> nodeGroupListGet($0.Empty request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$nodeGroupListGet, request, options: options);
  }

  $grpc.ResponseFuture<$1.MsgReply> nodeGroupSet($1.NodeGroup request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$nodeGroupSet, request, options: options);
  }

  $grpc.ResponseFuture<$1.MsgReply> nodeGroupDelete($1.ID32 request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$nodeGroupDelete, request, options: options);
  }

  $grpc.ResponseFuture<$1.NodeList> nodeListGet($0.Empty request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$nodeListGet, request, options: options);
  }

  $grpc.ResponseFuture<$1.MsgReply> nodeSet($1.Node request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$nodeSet, request, options: options);
  }

  $grpc.ResponseFuture<$1.MsgReply> nodeDelete($1.ID32 request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$nodeDelete, request, options: options);
  }

  $grpc.ResponseStream<$1.NodeStatus> nodeStatusStream($1.StreamRequest request,
      {$grpc.CallOptions? options}) {
    return $createStreamingCall(
        _$nodeStatusStream, $async.Stream.fromIterable([request]),
        options: options);
  }

  $grpc.ResponseFuture<$1.DomainCertificateList> domainCertificateListGet(
      $0.Empty request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$domainCertificateListGet, request,
        options: options);
  }

  $grpc.ResponseFuture<$1.MsgReply> domainCertificateSet(
      $1.DomainCertificate request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$domainCertificateSet, request, options: options);
  }

  $grpc.ResponseFuture<$1.MsgReply> domainCertificateDelete($1.ID32 request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$domainCertificateDelete, request,
        options: options);
  }

  $grpc.ResponseFuture<$1.Settings> settingsGet($0.Empty request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$settingsGet, request, options: options);
  }

  $grpc.ResponseFuture<$1.MsgReply> settingsSet($1.Settings request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$settingsSet, request, options: options);
  }

  $grpc.ResponseFuture<$1.UserList> userListGet($0.Empty request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$userListGet, request, options: options);
  }

  $grpc.ResponseFuture<$1.MsgReply> userSet($1.User request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$userSet, request, options: options);
  }

  $grpc.ResponseFuture<$1.MsgReply> userDelete($1.ID32 request,
      {$grpc.CallOptions? options}) {
    return $createUnaryCall(_$userDelete, request, options: options);
  }
}

abstract class AccWatchAPIServiceBase extends $grpc.Service {
  $core.String get $name => 'AccWatch.AccWatchAPI';

  AccWatchAPIServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.Empty, $1.NotificationPolicyList>(
        'NotificationPolicyListGet',
        notificationPolicyListGet_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Empty.fromBuffer(value),
        ($1.NotificationPolicyList value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$1.NotificationPolicy, $1.MsgReply>(
        'NotificationPolicySet',
        notificationPolicySet_Pre,
        false,
        false,
        ($core.List<$core.int> value) =>
            $1.NotificationPolicy.fromBuffer(value),
        ($1.MsgReply value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$1.ID32, $1.MsgReply>(
        'NotificationPolicyDelete',
        notificationPolicyDelete_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $1.ID32.fromBuffer(value),
        ($1.MsgReply value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Empty, $1.NetworkList>(
        'NetworkListGet',
        networkListGet_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Empty.fromBuffer(value),
        ($1.NetworkList value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$1.Network, $1.MsgReply>(
        'NetworkSet',
        networkSet_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $1.Network.fromBuffer(value),
        ($1.MsgReply value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$1.ID32, $1.MsgReply>(
        'NetworkDelete',
        networkDelete_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $1.ID32.fromBuffer(value),
        ($1.MsgReply value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$1.StreamRequest, $1.NetworkStatus>(
        'NetworkStatusStream',
        networkStatusStream_Pre,
        false,
        true,
        ($core.List<$core.int> value) => $1.StreamRequest.fromBuffer(value),
        ($1.NetworkStatus value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Empty, $1.NodeGroupList>(
        'NodeGroupListGet',
        nodeGroupListGet_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Empty.fromBuffer(value),
        ($1.NodeGroupList value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$1.NodeGroup, $1.MsgReply>(
        'NodeGroupSet',
        nodeGroupSet_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $1.NodeGroup.fromBuffer(value),
        ($1.MsgReply value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$1.ID32, $1.MsgReply>(
        'NodeGroupDelete',
        nodeGroupDelete_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $1.ID32.fromBuffer(value),
        ($1.MsgReply value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Empty, $1.NodeList>(
        'NodeListGet',
        nodeListGet_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Empty.fromBuffer(value),
        ($1.NodeList value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$1.Node, $1.MsgReply>(
        'NodeSet',
        nodeSet_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $1.Node.fromBuffer(value),
        ($1.MsgReply value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$1.ID32, $1.MsgReply>(
        'NodeDelete',
        nodeDelete_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $1.ID32.fromBuffer(value),
        ($1.MsgReply value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$1.StreamRequest, $1.NodeStatus>(
        'NodeStatusStream',
        nodeStatusStream_Pre,
        false,
        true,
        ($core.List<$core.int> value) => $1.StreamRequest.fromBuffer(value),
        ($1.NodeStatus value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Empty, $1.DomainCertificateList>(
        'DomainCertificateListGet',
        domainCertificateListGet_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Empty.fromBuffer(value),
        ($1.DomainCertificateList value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$1.DomainCertificate, $1.MsgReply>(
        'DomainCertificateSet',
        domainCertificateSet_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $1.DomainCertificate.fromBuffer(value),
        ($1.MsgReply value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$1.ID32, $1.MsgReply>(
        'DomainCertificateDelete',
        domainCertificateDelete_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $1.ID32.fromBuffer(value),
        ($1.MsgReply value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Empty, $1.Settings>(
        'SettingsGet',
        settingsGet_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Empty.fromBuffer(value),
        ($1.Settings value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$1.Settings, $1.MsgReply>(
        'SettingsSet',
        settingsSet_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $1.Settings.fromBuffer(value),
        ($1.MsgReply value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Empty, $1.UserList>(
        'UserListGet',
        userListGet_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Empty.fromBuffer(value),
        ($1.UserList value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$1.User, $1.MsgReply>(
        'UserSet',
        userSet_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $1.User.fromBuffer(value),
        ($1.MsgReply value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$1.ID32, $1.MsgReply>(
        'UserDelete',
        userDelete_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $1.ID32.fromBuffer(value),
        ($1.MsgReply value) => value.writeToBuffer()));
  }

  $async.Future<$1.NotificationPolicyList> notificationPolicyListGet_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Empty> request) async {
    return notificationPolicyListGet(call, await request);
  }

  $async.Future<$1.MsgReply> notificationPolicySet_Pre($grpc.ServiceCall call,
      $async.Future<$1.NotificationPolicy> request) async {
    return notificationPolicySet(call, await request);
  }

  $async.Future<$1.MsgReply> notificationPolicyDelete_Pre(
      $grpc.ServiceCall call, $async.Future<$1.ID32> request) async {
    return notificationPolicyDelete(call, await request);
  }

  $async.Future<$1.NetworkList> networkListGet_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Empty> request) async {
    return networkListGet(call, await request);
  }

  $async.Future<$1.MsgReply> networkSet_Pre(
      $grpc.ServiceCall call, $async.Future<$1.Network> request) async {
    return networkSet(call, await request);
  }

  $async.Future<$1.MsgReply> networkDelete_Pre(
      $grpc.ServiceCall call, $async.Future<$1.ID32> request) async {
    return networkDelete(call, await request);
  }

  $async.Stream<$1.NetworkStatus> networkStatusStream_Pre(
      $grpc.ServiceCall call, $async.Future<$1.StreamRequest> request) async* {
    yield* networkStatusStream(call, await request);
  }

  $async.Future<$1.NodeGroupList> nodeGroupListGet_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Empty> request) async {
    return nodeGroupListGet(call, await request);
  }

  $async.Future<$1.MsgReply> nodeGroupSet_Pre(
      $grpc.ServiceCall call, $async.Future<$1.NodeGroup> request) async {
    return nodeGroupSet(call, await request);
  }

  $async.Future<$1.MsgReply> nodeGroupDelete_Pre(
      $grpc.ServiceCall call, $async.Future<$1.ID32> request) async {
    return nodeGroupDelete(call, await request);
  }

  $async.Future<$1.NodeList> nodeListGet_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Empty> request) async {
    return nodeListGet(call, await request);
  }

  $async.Future<$1.MsgReply> nodeSet_Pre(
      $grpc.ServiceCall call, $async.Future<$1.Node> request) async {
    return nodeSet(call, await request);
  }

  $async.Future<$1.MsgReply> nodeDelete_Pre(
      $grpc.ServiceCall call, $async.Future<$1.ID32> request) async {
    return nodeDelete(call, await request);
  }

  $async.Stream<$1.NodeStatus> nodeStatusStream_Pre(
      $grpc.ServiceCall call, $async.Future<$1.StreamRequest> request) async* {
    yield* nodeStatusStream(call, await request);
  }

  $async.Future<$1.DomainCertificateList> domainCertificateListGet_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Empty> request) async {
    return domainCertificateListGet(call, await request);
  }

  $async.Future<$1.MsgReply> domainCertificateSet_Pre($grpc.ServiceCall call,
      $async.Future<$1.DomainCertificate> request) async {
    return domainCertificateSet(call, await request);
  }

  $async.Future<$1.MsgReply> domainCertificateDelete_Pre(
      $grpc.ServiceCall call, $async.Future<$1.ID32> request) async {
    return domainCertificateDelete(call, await request);
  }

  $async.Future<$1.Settings> settingsGet_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Empty> request) async {
    return settingsGet(call, await request);
  }

  $async.Future<$1.MsgReply> settingsSet_Pre(
      $grpc.ServiceCall call, $async.Future<$1.Settings> request) async {
    return settingsSet(call, await request);
  }

  $async.Future<$1.UserList> userListGet_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Empty> request) async {
    return userListGet(call, await request);
  }

  $async.Future<$1.MsgReply> userSet_Pre(
      $grpc.ServiceCall call, $async.Future<$1.User> request) async {
    return userSet(call, await request);
  }

  $async.Future<$1.MsgReply> userDelete_Pre(
      $grpc.ServiceCall call, $async.Future<$1.ID32> request) async {
    return userDelete(call, await request);
  }

  $async.Future<$1.NotificationPolicyList> notificationPolicyListGet(
      $grpc.ServiceCall call, $0.Empty request);
  $async.Future<$1.MsgReply> notificationPolicySet(
      $grpc.ServiceCall call, $1.NotificationPolicy request);
  $async.Future<$1.MsgReply> notificationPolicyDelete(
      $grpc.ServiceCall call, $1.ID32 request);
  $async.Future<$1.NetworkList> networkListGet(
      $grpc.ServiceCall call, $0.Empty request);
  $async.Future<$1.MsgReply> networkSet(
      $grpc.ServiceCall call, $1.Network request);
  $async.Future<$1.MsgReply> networkDelete(
      $grpc.ServiceCall call, $1.ID32 request);
  $async.Stream<$1.NetworkStatus> networkStatusStream(
      $grpc.ServiceCall call, $1.StreamRequest request);
  $async.Future<$1.NodeGroupList> nodeGroupListGet(
      $grpc.ServiceCall call, $0.Empty request);
  $async.Future<$1.MsgReply> nodeGroupSet(
      $grpc.ServiceCall call, $1.NodeGroup request);
  $async.Future<$1.MsgReply> nodeGroupDelete(
      $grpc.ServiceCall call, $1.ID32 request);
  $async.Future<$1.NodeList> nodeListGet(
      $grpc.ServiceCall call, $0.Empty request);
  $async.Future<$1.MsgReply> nodeSet($grpc.ServiceCall call, $1.Node request);
  $async.Future<$1.MsgReply> nodeDelete(
      $grpc.ServiceCall call, $1.ID32 request);
  $async.Stream<$1.NodeStatus> nodeStatusStream(
      $grpc.ServiceCall call, $1.StreamRequest request);
  $async.Future<$1.DomainCertificateList> domainCertificateListGet(
      $grpc.ServiceCall call, $0.Empty request);
  $async.Future<$1.MsgReply> domainCertificateSet(
      $grpc.ServiceCall call, $1.DomainCertificate request);
  $async.Future<$1.MsgReply> domainCertificateDelete(
      $grpc.ServiceCall call, $1.ID32 request);
  $async.Future<$1.Settings> settingsGet(
      $grpc.ServiceCall call, $0.Empty request);
  $async.Future<$1.MsgReply> settingsSet(
      $grpc.ServiceCall call, $1.Settings request);
  $async.Future<$1.UserList> userListGet(
      $grpc.ServiceCall call, $0.Empty request);
  $async.Future<$1.MsgReply> userSet($grpc.ServiceCall call, $1.User request);
  $async.Future<$1.MsgReply> userDelete(
      $grpc.ServiceCall call, $1.ID32 request);
}
