///
//  Generated code. Do not modify.
//  source: AccWatch.proto
//
// @dart = 2.12
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields,deprecated_member_use_from_same_package

import 'dart:core' as $core;
import 'dart:convert' as $convert;
import 'dart:typed_data' as $typed_data;
@$core.Deprecated('Use messageTypeDescriptor instead')
const MessageType$json = const {
  '1': 'MessageType',
  '2': const [
    const {'1': 'Add', '2': 0},
    const {'1': 'Update', '2': 1},
    const {'1': 'Remove', '2': 2},
  ],
};

/// Descriptor for `MessageType`. Decode as a `google.protobuf.EnumDescriptorProto`.
final $typed_data.Uint8List messageTypeDescriptor = $convert.base64Decode('CgtNZXNzYWdlVHlwZRIHCgNBZGQQABIKCgZVcGRhdGUQARIKCgZSZW1vdmUQAg==');
@$core.Deprecated('Use msgReplyDescriptor instead')
const MsgReply$json = const {
  '1': 'MsgReply',
  '2': const [
    const {'1': 'status', '3': 1, '4': 1, '5': 14, '6': '.AccWatch.MsgReply.Status', '10': 'status'},
    const {'1': 'message', '3': 2, '4': 1, '5': 9, '10': 'message'},
    const {'1': 'newID32', '3': 3, '4': 1, '5': 13, '10': 'newID32'},
  ],
  '4': const [MsgReply_Status$json],
};

@$core.Deprecated('Use msgReplyDescriptor instead')
const MsgReply_Status$json = const {
  '1': 'Status',
  '2': const [
    const {'1': 'fail', '2': 0},
    const {'1': 'ok', '2': 1},
  ],
};

/// Descriptor for `MsgReply`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List msgReplyDescriptor = $convert.base64Decode('CghNc2dSZXBseRIwCgZzdGF0dXMYASABKA4yGC5BY2N1Qm90Lk1zZ1JlcGx5LlN0YXR1c1IGc3RhdHVzEhgKB21lc3NhZ2UYAiABKAlSB21lc3NhZ2USGAoHbmV3SUQzMhgDIAEoDVIHbmV3SUQzMiIaCgZTdGF0dXMSCAoEZmFpbBAAEgYKAm9rEAE=');
@$core.Deprecated('Use iD32Descriptor instead')
const ID32$json = const {
  '1': 'ID32',
  '2': const [
    const {'1': 'ID', '3': 1, '4': 1, '5': 13, '10': 'ID'},
  ],
};

/// Descriptor for `ID32`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List iD32Descriptor = $convert.base64Decode('CgRJRDMyEg4KAklEGAEgASgNUgJJRA==');
@$core.Deprecated('Use streamRequestDescriptor instead')
const StreamRequest$json = const {
  '1': 'StreamRequest',
  '2': const [
    const {'1': 'Milliseconds', '3': 1, '4': 1, '5': 13, '10': 'Milliseconds'},
  ],
};

/// Descriptor for `StreamRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List streamRequestDescriptor = $convert.base64Decode('Cg1TdHJlYW1SZXF1ZXN0EiIKDE1pbGxpc2Vjb25kcxgBIAEoDVIMTWlsbGlzZWNvbmRz');
@$core.Deprecated('Use authenticationDescriptor instead')
const Authentication$json = const {
  '1': 'Authentication',
  '2': const [
    const {'1': 'UserID', '3': 1, '4': 1, '5': 13, '10': 'UserID'},
    const {'1': 'Password', '3': 2, '4': 1, '5': 9, '10': 'Password'},
    const {'1': 'TwoFAtype', '3': 3, '4': 1, '5': 14, '6': '.AccWatch.Authentication.twoFAtype', '10': 'TwoFAtype'},
    const {'1': 'TwoFAData', '3': 4, '4': 1, '5': 9, '10': 'TwoFAData'},
  ],
  '4': const [Authentication_twoFAtype$json],
};

@$core.Deprecated('Use authenticationDescriptor instead')
const Authentication_twoFAtype$json = const {
  '1': 'twoFAtype',
  '2': const [
    const {'1': 'Yubikey', '2': 0},
  ],
};

/// Descriptor for `Authentication`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List authenticationDescriptor = $convert.base64Decode('Cg5BdXRoZW50aWNhdGlvbhIWCgZVc2VySUQYASABKA1SBlVzZXJJRBIaCghQYXNzd29yZBgCIAEoCVIIUGFzc3dvcmQSPwoJVHdvRkF0eXBlGAMgASgOMiEuQWNjdUJvdC5BdXRoZW50aWNhdGlvbi50d29GQXR5cGVSCVR3b0ZBdHlwZRIcCglUd29GQURhdGEYBCABKAlSCVR3b0ZBRGF0YSIYCgl0d29GQXR5cGUSCwoHWXViaWtleRAA');
@$core.Deprecated('Use userDescriptor instead')
const User$json = const {
  '1': 'User',
  '2': const [
    const {'1': 'UserID', '3': 1, '4': 1, '5': 13, '10': 'UserID'},
    const {'1': 'Name', '3': 2, '4': 1, '5': 9, '10': 'Name'},
    const {'1': 'Email', '3': 3, '4': 1, '5': 9, '10': 'Email'},
    const {'1': 'Tel', '3': 4, '4': 1, '5': 9, '10': 'Tel'},
    const {'1': 'Discord', '3': 5, '4': 1, '5': 9, '10': 'Discord'},
    const {'1': 'AccessRightsBits', '3': 6, '4': 1, '5': 13, '10': 'AccessRightsBits'},
  ],
  '4': const [User_AccessRights$json],
};

@$core.Deprecated('Use userDescriptor instead')
const User_AccessRights$json = const {
  '1': 'AccessRights',
  '2': const [
    const {'1': 'Read', '2': 0},
    const {'1': 'Write', '2': 1},
    const {'1': 'Users', '2': 2},
    const {'1': 'Settings', '2': 4},
  ],
};

/// Descriptor for `User`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List userDescriptor = $convert.base64Decode('CgRVc2VyEhYKBlVzZXJJRBgBIAEoDVIGVXNlcklEEhIKBE5hbWUYAiABKAlSBE5hbWUSFAoFRW1haWwYAyABKAlSBUVtYWlsEhAKA1RlbBgEIAEoCVIDVGVsEhgKB0Rpc2NvcmQYBSABKAlSB0Rpc2NvcmQSKgoQQWNjZXNzUmlnaHRzQml0cxgGIAEoDVIQQWNjZXNzUmlnaHRzQml0cyI8CgxBY2Nlc3NSaWdodHMSCAoEUmVhZBAAEgkKBVdyaXRlEAESCQoFVXNlcnMQAhIMCghTZXR0aW5ncxAE');
@$core.Deprecated('Use userListDescriptor instead')
const UserList$json = const {
  '1': 'UserList',
  '2': const [
    const {'1': 'users', '3': 1, '4': 3, '5': 11, '6': '.AccWatch.User', '10': 'users'},
  ],
};

/// Descriptor for `UserList`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List userListDescriptor = $convert.base64Decode('CghVc2VyTGlzdBIjCgV1c2VycxgBIAMoCzINLkFjY3VCb3QuVXNlclIFdXNlcnM=');
@$core.Deprecated('Use notificationPolicyDescriptor instead')
const NotificationPolicy$json = const {
  '1': 'NotificationPolicy',
  '2': const [
    const {'1': 'NotifictionID', '3': 1, '4': 1, '5': 13, '10': 'NotifictionID'},
    const {'1': 'Name', '3': 2, '4': 1, '5': 9, '10': 'Name'},
    const {'1': 'Discord', '3': 3, '4': 1, '5': 5, '10': 'Discord'},
    const {'1': 'Call', '3': 4, '4': 1, '5': 5, '10': 'Call'},
  ],
};

/// Descriptor for `NotificationPolicy`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List notificationPolicyDescriptor = $convert.base64Decode('ChJOb3RpZmljYXRpb25Qb2xpY3kSJAoNTm90aWZpY3Rpb25JRBgBIAEoDVINTm90aWZpY3Rpb25JRBISCgROYW1lGAIgASgJUgROYW1lEhgKB0Rpc2NvcmQYAyABKAVSB0Rpc2NvcmQSEgoEQ2FsbBgEIAEoBVIEQ2FsbA==');
@$core.Deprecated('Use notificationPolicyListDescriptor instead')
const NotificationPolicyList$json = const {
  '1': 'NotificationPolicyList',
  '2': const [
    const {'1': 'NotificationPolicyList', '3': 1, '4': 3, '5': 11, '6': '.AccWatch.NotificationPolicy', '10': 'NotificationPolicyList'},
  ],
};

/// Descriptor for `NotificationPolicyList`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List notificationPolicyListDescriptor = $convert.base64Decode('ChZOb3RpZmljYXRpb25Qb2xpY3lMaXN0ElMKFk5vdGlmaWNhdGlvblBvbGljeUxpc3QYASADKAsyGy5BY2N1Qm90Lk5vdGlmaWNhdGlvblBvbGljeVIWTm90aWZpY2F0aW9uUG9saWN5TGlzdA==');
@$core.Deprecated('Use networkDescriptor instead')
const Network$json = const {
  '1': 'Network',
  '2': const [
    const {'1': 'NetworkID', '3': 1, '4': 1, '5': 13, '10': 'NetworkID'},
    const {'1': 'Name', '3': 2, '4': 1, '5': 9, '10': 'Name'},
    const {'1': 'BlockTime', '3': 3, '4': 1, '5': 13, '10': 'BlockTime'},
    const {'1': 'StalledAfter', '3': 4, '4': 1, '5': 13, '10': 'StalledAfter'},
    const {'1': 'NotifictionID', '3': 5, '4': 1, '5': 13, '10': 'NotifictionID'},
  ],
};

/// Descriptor for `Network`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List networkDescriptor = $convert.base64Decode('CgdOZXR3b3JrEhwKCU5ldHdvcmtJRBgBIAEoDVIJTmV0d29ya0lEEhIKBE5hbWUYAiABKAlSBE5hbWUSHAoJQmxvY2tUaW1lGAMgASgNUglCbG9ja1RpbWUSIgoMU3RhbGxlZEFmdGVyGAQgASgNUgxTdGFsbGVkQWZ0ZXISJAoNTm90aWZpY3Rpb25JRBgFIAEoDVINTm90aWZpY3Rpb25JRA==');
@$core.Deprecated('Use networkListDescriptor instead')
const NetworkList$json = const {
  '1': 'NetworkList',
  '2': const [
    const {'1': 'network', '3': 1, '4': 3, '5': 11, '6': '.AccWatch.Network', '10': 'network'},
  ],
};

/// Descriptor for `NetworkList`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List networkListDescriptor = $convert.base64Decode('CgtOZXR3b3JrTGlzdBIqCgduZXR3b3JrGAEgAygLMhAuQWNjdUJvdC5OZXR3b3JrUgduZXR3b3Jr');
@$core.Deprecated('Use networkStatusDescriptor instead')
const NetworkStatus$json = const {
  '1': 'NetworkStatus',
  '2': const [
    const {'1': 'NetworkID', '3': 1, '4': 1, '5': 13, '10': 'NetworkID'},
    const {'1': 'Height', '3': 2, '4': 1, '5': 4, '10': 'Height'},
    const {'1': 'AverageTime', '3': 3, '4': 1, '5': 2, '10': 'AverageTime'},
  ],
};

/// Descriptor for `NetworkStatus`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List networkStatusDescriptor = $convert.base64Decode('Cg1OZXR3b3JrU3RhdHVzEhwKCU5ldHdvcmtJRBgBIAEoDVIJTmV0d29ya0lEEhYKBkhlaWdodBgCIAEoBFIGSGVpZ2h0EiAKC0F2ZXJhZ2VUaW1lGAMgASgCUgtBdmVyYWdlVGltZQ==');
@$core.Deprecated('Use nodeGroupDescriptor instead')
const NodeGroup$json = const {
  '1': 'NodeGroup',
  '2': const [
    const {'1': 'NodeGroupID', '3': 1, '4': 1, '5': 13, '10': 'NodeGroupID'},
    const {'1': 'Name', '3': 2, '4': 1, '5': 9, '10': 'Name'},
    const {'1': 'NetworkID', '3': 3, '4': 1, '5': 13, '10': 'NetworkID'},
    const {'1': 'PingNotifictionID', '3': 4, '4': 1, '5': 13, '10': 'PingNotifictionID'},
    const {'1': 'HeightNotifictionID', '3': 5, '4': 1, '5': 13, '10': 'HeightNotifictionID'},
    const {'1': 'LatencyNotifictionID', '3': 6, '4': 1, '5': 13, '10': 'LatencyNotifictionID'},
  ],
};

/// Descriptor for `NodeGroup`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List nodeGroupDescriptor = $convert.base64Decode('CglOb2RlR3JvdXASIAoLTm9kZUdyb3VwSUQYASABKA1SC05vZGVHcm91cElEEhIKBE5hbWUYAiABKAlSBE5hbWUSHAoJTmV0d29ya0lEGAMgASgNUglOZXR3b3JrSUQSLAoRUGluZ05vdGlmaWN0aW9uSUQYBCABKA1SEVBpbmdOb3RpZmljdGlvbklEEjAKE0hlaWdodE5vdGlmaWN0aW9uSUQYBSABKA1SE0hlaWdodE5vdGlmaWN0aW9uSUQSMgoUTGF0ZW5jeU5vdGlmaWN0aW9uSUQYBiABKA1SFExhdGVuY3lOb3RpZmljdGlvbklE');
@$core.Deprecated('Use nodeGroupListDescriptor instead')
const NodeGroupList$json = const {
  '1': 'NodeGroupList',
  '2': const [
    const {'1': 'nodeGroup', '3': 1, '4': 3, '5': 11, '6': '.AccWatch.NodeGroup', '10': 'nodeGroup'},
  ],
};

/// Descriptor for `NodeGroupList`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List nodeGroupListDescriptor = $convert.base64Decode('Cg1Ob2RlR3JvdXBMaXN0EjAKCW5vZGVHcm91cBgBIAMoCzISLkFjY3VCb3QuTm9kZUdyb3VwUglub2RlR3JvdXA=');
@$core.Deprecated('Use nodeDescriptor instead')
const Node$json = const {
  '1': 'Node',
  '2': const [
    const {'1': 'NodeID', '3': 1, '4': 1, '5': 13, '10': 'NodeID'},
    const {'1': 'NodeGroupID', '3': 2, '4': 1, '5': 13, '10': 'NodeGroupID'},
    const {'1': 'Name', '3': 3, '4': 1, '5': 9, '10': 'Name'},
    const {'1': 'Host', '3': 4, '4': 1, '5': 9, '10': 'Host'},
    const {'1': 'Monitor', '3': 5, '4': 1, '5': 8, '10': 'Monitor'},
  ],
};

/// Descriptor for `Node`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List nodeDescriptor = $convert.base64Decode('CgROb2RlEhYKBk5vZGVJRBgBIAEoDVIGTm9kZUlEEiAKC05vZGVHcm91cElEGAIgASgNUgtOb2RlR3JvdXBJRBISCgROYW1lGAMgASgJUgROYW1lEhIKBEhvc3QYBCABKAlSBEhvc3QSGAoHTW9uaXRvchgFIAEoCFIHTW9uaXRvcg==');
@$core.Deprecated('Use nodeStatusDescriptor instead')
const NodeStatus$json = const {
  '1': 'NodeStatus',
  '2': const [
    const {'1': 'NodeID', '3': 1, '4': 1, '5': 13, '10': 'NodeID'},
    const {'1': 'Version', '3': 2, '4': 1, '5': 9, '10': 'Version'},
    const {'1': 'Height', '3': 3, '4': 1, '5': 4, '10': 'Height'},
    const {'1': 'Ping', '3': 4, '4': 1, '5': 2, '10': 'Ping'},
  ],
};

/// Descriptor for `NodeStatus`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List nodeStatusDescriptor = $convert.base64Decode('CgpOb2RlU3RhdHVzEhYKBk5vZGVJRBgBIAEoDVIGTm9kZUlEEhgKB1ZlcnNpb24YAiABKAlSB1ZlcnNpb24SFgoGSGVpZ2h0GAMgASgEUgZIZWlnaHQSEgoEUGluZxgEIAEoAlIEUGluZw==');
@$core.Deprecated('Use nodeListDescriptor instead')
const NodeList$json = const {
  '1': 'NodeList',
  '2': const [
    const {'1': 'nodes', '3': 1, '4': 3, '5': 11, '6': '.AccWatch.Node', '10': 'nodes'},
  ],
};

/// Descriptor for `NodeList`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List nodeListDescriptor = $convert.base64Decode('CghOb2RlTGlzdBIjCgVub2RlcxgBIAMoCzINLkFjY3VCb3QuTm9kZVIFbm9kZXM=');
@$core.Deprecated('Use domainCertificateDescriptor instead')
const DomainCertificate$json = const {
  '1': 'DomainCertificate',
  '2': const [
    const {'1': 'DomainCertificateID', '3': 1, '4': 1, '5': 13, '10': 'DomainCertificateID'},
    const {'1': 'Domain', '3': 2, '4': 1, '5': 9, '10': 'Domain'},
    const {'1': 'Monitor', '3': 3, '4': 1, '5': 8, '10': 'Monitor'},
  ],
};

/// Descriptor for `DomainCertificate`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List domainCertificateDescriptor = $convert.base64Decode('ChFEb21haW5DZXJ0aWZpY2F0ZRIwChNEb21haW5DZXJ0aWZpY2F0ZUlEGAEgASgNUhNEb21haW5DZXJ0aWZpY2F0ZUlEEhYKBkRvbWFpbhgCIAEoCVIGRG9tYWluEhgKB01vbml0b3IYAyABKAhSB01vbml0b3I=');
@$core.Deprecated('Use domainCertificateListDescriptor instead')
const DomainCertificateList$json = const {
  '1': 'DomainCertificateList',
  '2': const [
    const {'1': 'domainCertificate', '3': 1, '4': 3, '5': 11, '6': '.AccWatch.DomainCertificate', '10': 'domainCertificate'},
  ],
};

/// Descriptor for `DomainCertificateList`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List domainCertificateListDescriptor = $convert.base64Decode('ChVEb21haW5DZXJ0aWZpY2F0ZUxpc3QSSAoRZG9tYWluQ2VydGlmaWNhdGUYASADKAsyGi5BY2N1Qm90LkRvbWFpbkNlcnRpZmljYXRlUhFkb21haW5DZXJ0aWZpY2F0ZQ==');
@$core.Deprecated('Use settingsDescriptor instead')
const Settings$json = const {
  '1': 'Settings',
  '2': const [
    const {'1': 'BotName', '3': 1, '4': 1, '5': 9, '10': 'BotName'},
    const {'1': 'DiscordClientID', '3': 2, '4': 1, '5': 9, '10': 'DiscordClientID'},
    const {'1': 'DiscordToken', '3': 3, '4': 1, '5': 9, '10': 'DiscordToken'},
    const {'1': 'AccumulateOperatorAlertsCh', '3': 4, '4': 1, '5': 4, '10': 'AccumulateOperatorAlertsCh'},
    const {'1': 'DiscordAlertsChannel', '3': 5, '4': 1, '5': 9, '10': 'DiscordAlertsChannel'},
    const {'1': 'SIPUsername', '3': 6, '4': 1, '5': 9, '10': 'SIPUsername'},
    const {'1': 'SIPPassword', '3': 7, '4': 1, '5': 9, '10': 'SIPPassword'},
    const {'1': 'SIPHost', '3': 8, '4': 1, '5': 9, '10': 'SIPHost'},
    const {'1': 'SIPCallingNumber', '3': 9, '4': 1, '5': 9, '10': 'SIPCallingNumber'},
    const {'1': 'TwimletURL', '3': 10, '4': 1, '5': 9, '10': 'TwimletURL'},
    const {'1': 'AlarmOffWarningMinutes', '3': 11, '4': 1, '5': 13, '10': 'AlarmOffWarningMinutes'},
    const {'1': 'LatencyTriggerMultiplier', '3': 12, '4': 1, '5': 13, '10': 'LatencyTriggerMultiplier'},
    const {'1': 'BotCommandPrefix', '3': 13, '4': 1, '5': 9, '10': 'BotCommandPrefix'},
    const {'1': 'EmailSMTPHost', '3': 14, '4': 1, '5': 9, '10': 'EmailSMTPHost'},
    const {'1': 'EmailSMTPPort', '3': 15, '4': 1, '5': 13, '10': 'EmailSMTPPort'},
    const {'1': 'EmailUsername', '3': 16, '4': 1, '5': 9, '10': 'EmailUsername'},
    const {'1': 'EmailPassword', '3': 17, '4': 1, '5': 9, '10': 'EmailPassword'},
    const {'1': 'EmailFromAddress', '3': 18, '4': 1, '5': 9, '10': 'EmailFromAddress'},
  ],
};

/// Descriptor for `Settings`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List settingsDescriptor = $convert.base64Decode('CghTZXR0aW5ncxIYCgdCb3ROYW1lGAEgASgJUgdCb3ROYW1lEigKD0Rpc2NvcmRDbGllbnRJRBgCIAEoCVIPRGlzY29yZENsaWVudElEEiIKDERpc2NvcmRUb2tlbhgDIAEoCVIMRGlzY29yZFRva2VuEj4KGkFjY3VtdWxhdGVPcGVyYXRvckFsZXJ0c0NoGAQgASgEUhpBY2N1bXVsYXRlT3BlcmF0b3JBbGVydHNDaBIyChREaXNjb3JkQWxlcnRzQ2hhbm5lbBgFIAEoCVIURGlzY29yZEFsZXJ0c0NoYW5uZWwSIAoLU0lQVXNlcm5hbWUYBiABKAlSC1NJUFVzZXJuYW1lEiAKC1NJUFBhc3N3b3JkGAcgASgJUgtTSVBQYXNzd29yZBIYCgdTSVBIb3N0GAggASgJUgdTSVBIb3N0EioKEFNJUENhbGxpbmdOdW1iZXIYCSABKAlSEFNJUENhbGxpbmdOdW1iZXISHgoKVHdpbWxldFVSTBgKIAEoCVIKVHdpbWxldFVSTBI2ChZBbGFybU9mZldhcm5pbmdNaW51dGVzGAsgASgNUhZBbGFybU9mZldhcm5pbmdNaW51dGVzEjoKGExhdGVuY3lUcmlnZ2VyTXVsdGlwbGllchgMIAEoDVIYTGF0ZW5jeVRyaWdnZXJNdWx0aXBsaWVyEioKEEJvdENvbW1hbmRQcmVmaXgYDSABKAlSEEJvdENvbW1hbmRQcmVmaXgSJAoNRW1haWxTTVRQSG9zdBgOIAEoCVINRW1haWxTTVRQSG9zdBIkCg1FbWFpbFNNVFBQb3J0GA8gASgNUg1FbWFpbFNNVFBQb3J0EiQKDUVtYWlsVXNlcm5hbWUYECABKAlSDUVtYWlsVXNlcm5hbWUSJAoNRW1haWxQYXNzd29yZBgRIAEoCVINRW1haWxQYXNzd29yZBIqChBFbWFpbEZyb21BZGRyZXNzGBIgASgJUhBFbWFpbEZyb21BZGRyZXNz');
