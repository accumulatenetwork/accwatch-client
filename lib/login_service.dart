import 'package:grpc/grpc_web.dart';
import 'package:grpc_web_ks/gRPC/authentication.pb.dart';
import 'package:grpc_web_ks/gRPC/google/protobuf/empty.pb.dart';

import 'gRPC/AccWatch.pbgrpc.dart';
import 'gRPC/authentication.pbgrpc.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:grpc_web_ks/global.dart';

class LoginService{
    final channel =
        GrpcWebClientChannel.xhr(Uri.parse(apiURL));

    Future<bool> login({String username, String password})async{
        final client = AccWatchAuthenticationClient(channel);
        try{
          AuthenticationReply result = await client.authenticate(AuthenticationRequest(username: username,password: password));
        if(result.expiresIn>0)
        {
          final prefs = await SharedPreferences.getInstance();
          await prefs.setString('AccessToken', result.accessToken);
          await prefs.setInt("expiresIn", result.expiresIn);
          return true;
        }
        else return false;
        }catch(e){
          return false;
        }
    }

    
}