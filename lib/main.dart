import 'package:flutter/material.dart';
import 'package:grpc_web_ks/provider/menu_provider.dart';
import 'package:grpc_web_ks/screens/login.dart';
import 'package:grpc_web_ks/widget/custom_scroll.dart';
import 'package:grpc_web_ks/global.dart';
import 'package:provider/provider.dart';
import 'package:grpc_web_ks/provider/data_provider.dart';


void main() {

    // grpc connecton is same as host, unless <url>?grpc="host:port" is provided.
    apiURL = Uri.base.queryParameters["grpc"] ?? Uri.base.toString();
    if (!apiURL.startsWith("http")) apiURL = "https://"+apiURL;
    print("grpc host = "+apiURL);

    runApp(MultiProvider(
    providers: [
      ChangeNotifierProvider(create: (_) => DataProvider()),
      ChangeNotifierProvider(create: (_) => MenuProvider()),
    ],
    child: MyApp()
  ));}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'AccWatch Client',
      scrollBehavior: MyCustomScrollBehavior(),
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: LoginPage(),
    );
  }
}