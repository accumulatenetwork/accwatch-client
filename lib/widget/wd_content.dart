import 'package:flutter/material.dart';

import '../gRPC/AccWatch.pb.dart';
  
  BoxDecoration boxDecoration(int position) {
    return BoxDecoration(
        color: position==0?Colors.blueGrey.shade100:Colors.transparent,
        border: Border(
            top: position == 1
                ? BorderSide.none
                : BorderSide(color: Colors.grey),
            right: BorderSide(color: Colors.grey),
            bottom: BorderSide(color: Colors.grey),
            left: BorderSide(color: Colors.grey)));
  }

  Widget wdMainTitle(String value){
    return Container(
                    alignment: Alignment.topLeft,
                    padding: EdgeInsets.only(left:15,top: 10,bottom:0),
                    child: Text(value,style: TextStyle(color: Colors.blue,fontSize: 25)));
  }

  Widget wdContent(int type,String value) {
    return GestureDetector(
      child: Container(
        decoration: boxDecoration(1),
        width: 150,
        height: 25,
        alignment: Alignment.center,
        padding: const EdgeInsets.only(left: 5),
        child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: Text(value),
        ),
      ),
    );
  }

    Widget wdContentEmpty() {
    return GestureDetector(
      child: Container(
        decoration: boxDecoration(1),
        width: 150,
        height: 25,
        alignment: Alignment.center,
        padding: const EdgeInsets.only(left: 5),
      ),
      onTap: () async {
      },
    );
  }

  Widget wdCheckContent(int type,String value) {
    return GestureDetector(
      child: Container(
        decoration: boxDecoration(1),
        width: 150,
        height: 25,
        alignment: Alignment.center,
        child: Icon(value == "true"
            ? Icons.check_box_outlined
            : Icons.check_box_outline_blank),
      ),
    );
  }

  Widget wdPlusBtn(Function onChange){
    return Container(
              alignment: Alignment.bottomRight,
              child: GestureDetector(
                onTap: onChange,
                child: Container(
                  height: 60,width: 60,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(30),
                    color: Colors.blue[600]
                  ),
                  child: Icon(Icons.add,color: Colors.white,size: 30,),),),
            );
  }