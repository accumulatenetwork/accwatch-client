import 'package:flutter/material.dart';
import 'package:grpc_web_ks/notification_data.dart';
import 'package:grpc_web_ks/provider/data_provider.dart';
import 'package:grpc_web_ks/provider/menu_provider.dart';
import 'package:provider/provider.dart';

class NavDrawer extends StatelessWidget {
  const NavDrawer({Key key,this.onChange,this.selectedIndex}) : super(key: key);

  final Function onChange;
  final int selectedIndex;

  @override
  Widget build(BuildContext context) {
    
    return Drawer(
      child: ListView(children: [
                ...tabTitle.asMap().map((key,value)=>MapEntry(key, ListTile(
                leading: Icon(Icons.input),
                title: Text(value.toString()),
                tileColor: selectedIndex==key?Colors.blue[800]:Colors.white,
                onTap:(){
                  onChange(key);
                }
              ))).values.toList(),
      ]));
  }
}