List<Map<String, Object>> nodes = [];

const tabTitle = [
  "Nodes","NodeGroups","SSLCerts","Users","Settings"
];

const nodeTitle=[
  "Group","Name","Host","Monitor","Version","Height","Ping"
];

const userTitle=[
  "Discord ID","Name","Tel","E-mail"
];

const nodeGrupTitle=[
  "Node Group","Network","Ping","Height","Latency"
];

const networkTitle=[
  "Network Name","Block Time(sec)","Stalled After(sec)","Stall Notification","Height","AverageTime"
];

const notificationPoliyTitle = [
  "Name","Discord","Call"
];

const sslCertTitle = [
  "URL","Contacts","Monitor"
];

const settingTitle = [
  "Setting","Value","Comments"
];

const settingKey = [
  "BotName",
  "Discord-ClientID",
  "Discord-Token",
  "Factom-OperatorAlertsCh",
  "Discord-AlertsChannel",
  "1SIP-Username",
  "1SIP-Password",
  "1SIP-Host",
  "SIP-CallingNumber",
  "TwimletURL",
  "AlarmOffWarningMinutes",
  "LatencyTriggerMultiplier",
  "SIP-Username",
  "SIP-Password",
  "SIP-Host",
  "BotCommandPrefix",
  "Email-SMTPHost",
  "Email-SMTPPort",
  "Email-Username",
  "Email-Password",
  "Email-FromAddress"
];

const SettingComment=[
  "Choose a unique name, for your organisation",
  "",
  "",
  "Factorm #Operator-alerts channel id ",
  "must match a channel in your own Discord Bot",
  "",
  "This can be set with an environment var if prefered",
  "",
  "Required for twilio",
  "",
  "X times a nodes lowest result",
  "",
  "",
  "This can be set with an environment var if prefered",
  "",
  "",
  "ip/domain of your SMTP server",
  "",
  "",
  "",
  ""
];