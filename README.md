# Accwatch Flutter Frontend


Uses gRPC to communicate with [AccWatch host](https://gitlab.com/accumulatenetwork/monitoring/accwatch).


#### Developers
    gRPC API port defaults to the host URL which serves up the client app. 
    You can override the gRPC port by adding url arg:  ?grpc=<host:port>
    e.g: https://myhost.com?grpc=localhost:8080

    CORS may need to be set.